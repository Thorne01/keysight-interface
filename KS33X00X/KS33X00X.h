#ifndef __KS33X00X__
#define __KS33X00X__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define KEYSIGHT_MSG_SIZE 512
#define KEYSIGHT_COM_ERR 1
#define KEYSIGHT_SUCCESS 0
#define KEYSIGHT_ERR 1
#define KEYSIGHT_FAILURE -1
#define KEYSIGHT_CONNECT_FAIL -1

struct usbtmc_attribute {
    int attribute;
    int value;
};

const int KS33X00X_connect(const char* port_id);
int KS33X00X_disconnect(const int);

int KS33X00X_write_msg(const int serial_port, char* msg, char* error_msg);
int KS33X00X_write_msg_fast(const int serial_port, char* msg, char* error_msg);
int KS33X00X_read_msg(const int serial_port, char* reply, char* error_msg);

int KS33X00X_set_reset(const int serial_port, char* error);
int KS33X00X_get_error_msg(const int serial_port, char* reply);
int KS33X00X_get_identify(const int serial_port, char* reply, char* error);
int KS33X00X_abort(const int serial_port, char* reply);
int KS33X00X_OPC(const int serial_port, char* reply);
int KS33X00X_load_arbitrary_function(const int serial_port, int source, char* memory_location_local_or_usb, char* error);

int KS33X00X_set_function(const int serial_port, int source, char* function, char* error);
int KS33X00X_set_output(const int serial_port, int source, int state, char* error);
int KS33X00X_set_output_load(const int serial_port, int source, int load, char* error);

int KS33X00X_get_function(const int serial_port, int source, char* function, char* error);
int KS33X00X_get_output(const int serial_port, int source, int* state, char* error);
int KS33X00X_get_output_load(const int serial_port, int source, double* load, char* error);

int KS33X00X_set_voltage_amplitude(const int serial_port, int source, double v_amp, char* error);
int KS33X00X_set_voltage_high(const int serial_port, int source, double v_high, char* error);
int KS33X00X_set_voltage_low(const int serial_port, int source, double v_low, char* error);
int KS33X00X_set_voltage_offset(const int serial_port, int source, double v_offset, char* error);

int KS33X00X_get_voltage_amplitude(const int serial_port, int source, double* voltage, char* error);
int KS33X00X_get_voltage_high(const int serial_port, int source, double* voltage_high, char* error);
int KS33X00X_get_voltage_low(const int serial_port, int source, double* voltage_low, char* error);
int KS33X00X_get_voltage_offset(const int serial_port, int source, double* offset, char* error);

int KS33X00X_set_burst_state(const int serial_port, int source, int on_off, char* error);
int KS33X00X_set_burst_mode(const int serial_port, int source, int mode, char* error);
int KS33X00X_set_burst_count(const int serial_port, int source, double count, char* error);
int KS33X00X_set_burst_period(const int serial_port, int source, double seconds, char* error);

int KS33X00X_get_burst_state(const int serial_port, int source, int* on_off, char* error);
int KS33X00X_get_burst_mode(const int serial_port, int source, char* mode, char* error);
int KS33X00X_get_burst_count(const int serial_port, int source, double* count, char* error);
int KS33X00X_get_burst_period(const int serial_port, int source, double* seconds, char* error);

int KS33X00X_set_frequency(const int serial_port, int source, double frequency, char* error);
int KS33X00X_set_phase(const int serial_port, int source, double frequency, char* error);
int KS33X00X_set_zero_phase(const int serial_port, int source, char* error);
int KS33X00X_set_phase_sync(const int serial_port, char* error);

int KS33X00X_get_frequency(const int serial_port, int source, double* frequency, char* error);
int KS33X00X_get_phase(const int serial_port, int source, double* phase, char* error);

int KS33X00X_set_arbitrary_function(const int serial_port, int source, char* function, char* error);
int KS33X00X_set_arbitrary_sample_rate(const int serial_port, int source, double sample_rate, char* error);
int KS33X00X_set_arbitrary_function_name(const int serial_port, int source, const char* name, const char* data, char* error);

int KS33X00X_get_arbitrary_sample_rate(const int serial_port, int source, double* sample_rate, char* error);
int KS33X00X_get_arbitrary_function_name(const int serial_port, int source, char* name, char* error);

int KS33X00X_set_AM_depth(const int serial_port, int source, double percentage, char* error);
int KS33X00X_set_AM_source(const int serial_port, int output_source, char* input_source, char* error);
int KS33X00X_set_AM_function(const int serial_port, int source, char* function, char* error);
int KS33X00X_set_AM_frequency(const int serial_port, int source, double frequency, char* error);
int KS33X00X_set_AM_DSSC(const int serial_port, int source, int state, char* error);
int KS33X00X_set_AM_state(const int serial_port, int source, int state, char* error);

int KS33X00X_get_AM_depth(const int serial_port, int source, double* percentage, char* error);
int KS33X00X_get_AM_state(const int serial_port, int source, int* state, char* error);
int KS33X00X_get_AM_DSSC(const int serial_port, int source, int* state, char* error);
int KS33X00X_get_AM_frequency(const int serial_port, int source, double* frequency, char* error);
int KS33X00X_get_AM_function(const int serial_port, int source, char* AM_function, char* error);
int KS33X00X_get_AM_source(const int serial_port, int output_source, char* input_source, char* error);

int KS33X00X_set_trigger(const int serial_port, int source, char* error);
int KS33X00X_set_trigger_count(const int serial_port, int source, int count, char* error);
int KS33X00X_set_trigger_delay(const int serial_port, int source, double seconds, char* error);
int KS33X00X_set_trigger_slope(const int serial_port, int source, char* slope, char* error);
int KS33X00X_set_trigger_source(const int serial_port, int source, char* trigger_input, char* error);
int KS33X00X_set_trigger_timer(const int serial_port, int source, double trigger_timer, char* error);
int KS33X00X_set_trigger_level(const int serial_port, int source, double voltage, char* error);

int KS33X00X_get_trigger_count(const int serial_port, int source, int* count, char* error);
int KS33X00X_get_trigger_delay(const int serial_port, int source, double* seconds, char* error);
int KS33X00X_get_trigger_slope(const int serial_port, int source, char* slope, char* error);
int KS33X00X_get_trigger_source(const int serial_port, int source, char* trigger_input, char* error);
int KS33X00X_get_trigger_timer(const int serial_port, int source, double* trigger_timer, char* error);
int KS33X00X_get_trigger_level(const int serial_port, int source, double* voltage, char* error); //set and get of trigger level only work for KS33600A

int KS33X00X_set_pulse_width(const int serial_port, int source, double width_sec, char* error);
int KS33X00X_get_pulse_width(const int serial_port, int source, double* width_sec, char* error);

int KS33X00X_set_ROsc_auto(const int serial_port, int on_off, char* error);
int KS33X00X_set_ROsc_source(const int serial_port, char* source, char* error);

int KS33X00X_get_ROsc_auto(const int serial_port, char* state, char* error);
int KS33X00X_get_ROsc_source(const int serial_port, char* source, char* error);

int KS33X00X_set_custom_command(const int serial_port, char* string, char* error);
int KS33X00X_get_custom_command(const int serial_port, char* string, char* reply, char* error);

int KS33X00X_set_sf_parameters(const int serial_port, int source, double frequency, double amplitude, double phase, int output_state, int AM_state, char* error);
int KS33X00X_set_mod_parameters(const int serial_port, int source, double sample_rate, int number_of_samples, double t0, double t1, double t2, double order, char* waveform_name, double amplitude, char* error);

int KS33X00X_clear_volatile_memory(const int serial_port, int source, char* error);

double SF_Damped(double t, double t0, double t1, double t2, double order);

#endif
