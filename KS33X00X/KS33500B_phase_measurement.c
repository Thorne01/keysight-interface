//
// Created by jacobthorne on 04/02/2020.
//

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "KS33X00X.h"

double SF_Damped(double t, double t0, double t1, double t2, double order) {

    if (t < t0)

        return 1;

    else {
        if (t < t2) {
            return pow((t0 - t1) / (t - t1), order);
        } else {

                return 0;
            
        }

    }
}

int main(void) {
    printf("\n +----------------------------------+");
    printf("\n |        Serial Port Read          |");
    printf("\n +----------------------------------+\n");
    const int fd1 = KS33500B_connect("/dev/usbtmc0");
    if (fd1 == -1) {
        printf("\nERROR CONNECTING!\n");
        return -1;
    }

    const int fd2 = KS33500B_connect("/dev/usbtmc1");
    if (fd2 == -1) {
        printf("\nERROR CONNECTING!\n");
        return -1;
    }

    const int fd3 = KS33500B_connect("/dev/usbtmc2");
    if (fd3 == -1) {
        printf("\nERROR CONNECTING!\n");
        return -1;
    }

    const int fd4 = KS33500B_connect("/dev/usbtmc3");
    if (fd4 == -1) {
        printf("\nERROR CONNECTING!\n");
        return -1;
    }

    char error[KEYSIGHT_MSG_SIZE];

    if (KS33500B_set_reset(fd1, error))
    {
        printf("Error: %s", error);
        KS33500B_get_error_msg(fd1, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33500B_set_reset(fd2, error))
    {
        printf("Error: %s", error);
        KS33500B_get_error_msg(fd2, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33500B_set_reset(fd3, error))
    {
        printf("Error: %s", error);
        KS33500B_get_error_msg(fd3, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33500B_set_reset(fd4, error))
    {
        printf("Error: %s", error);
        KS33500B_get_error_msg(fd3, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }

    char reply[KEYSIGHT_MSG_SIZE];

    if (KS33500B_get_identify(fd1, reply, error))
    {
        printf("Error: %s", error);
        KS33500B_get_error_msg(fd1, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("ID device 1: %s", reply);

    if (KS33500B_get_identify(fd2, reply, error))
    {
        printf("Error: %s", error);
        KS33500B_get_error_msg(fd2, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("ID device 2: %s", reply);

    if (KS33500B_get_identify(fd3, reply, error))
    {
        printf("Error: %s", error);
        KS33500B_get_error_msg(fd3, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("ID device 3: %s", reply);

    if (KS33500B_get_identify(fd4, reply, error))
    {
        printf("Error: %s", error);
        KS33500B_get_error_msg(fd3, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("ID device 4: %s", reply);

    //**************************Device initialise settings**************************

    if(KS33500B_set_ROsc_source(fd1, "INTernal", error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_ROsc_source(fd2, "EXTernal", error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_ROsc_source(fd4, "EXTernal", error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_output_load(fd1, 1, 0, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_output_load(fd2, 1, 0, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_output_load(fd3, 1, 0, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_output_load(fd4, 1, 0, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    //**************************USBTMC0 initialise settings**************************

    //char* arb = "\"INT:\\BUILTIN\\EXP_FALL.ARB\""; //example string for loading a function in waveform generators memory

    if(KS33500B_set_trigger_source(fd1, 1, "EXTernal", error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_trigger_delay(fd1, 1, 0, error)){
            printf("Error: %s", error);
            return KEYSIGHT_FAILURE;
        }

    if(KS33500B_set_trigger_slope(fd1, 1, "POS", error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_burst_mode(fd1, 1, 1, error))//triggered mode
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_burst_count(fd1, 1, 1, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_burst_state(fd1, 1, 1, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

//    if(KS33500B_load_arbitrary_function(fd1, 1, arb, error)){
//        printf("Error: %s", error);
//        return KEYSIGHT_FAILURE;
//    }
//
//    if(KS33500B_set_function(fd1, 1, "ARB", error)) {
//        printf("Error: %s", error);
//        return KEYSIGHT_FAILURE;
//    }
//
//    if(KS33500B_set_arbitrary_function(fd1, 1, arb, error)){
//        printf("Error: %s", error);
//        return KEYSIGHT_FAILURE;
//    }

    double sample_rate_in = 50000;
    if(KS33500B_set_arbitrary_sample_rate(fd1, 1, sample_rate_in, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    double sample_rate_out = 0; //checks the set sample rate then passes this to the function to make
    if(KS33500B_get_arbitrary_sample_rate(fd1, 1, &sample_rate_out, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    //printf("Set sample rate: %f\n",sample_rate_out);

    int length = 0;

    const int num_samples = 2000;

    const int samples_per_second = (int)sample_rate_out;

    char arbFuncDataArray[(6 + 2) * num_samples];

    for (int s = 0; s < num_samples; ++s) {

        double t = (double)(((double) s) / ((double)samples_per_second));

        double func_val = SF_Damped(t,0.005, 0.0005,0.034, 1.);

        int adc_val = func_val > 0 ? (int) floor(func_val * 65535 / 2) : (int) ceil(func_val * 65535 / 2);

        char cval[KEYSIGHT_MSG_SIZE];
        memset(cval, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(cval, "%d", adc_val);

        if (length + strlen(cval) < sizeof(arbFuncDataArray)) {

            length += sprintf(arbFuncDataArray + length, "%s,", cval);
        }
    }

//    length += sprintf(arbFuncDataArray + length, "%s,", "0");

//    sprintf(arbFuncDataArray + length, "%s,", "0");

    size_t indexOfNullTerminator = strlen(arbFuncDataArray);

    arbFuncDataArray[indexOfNullTerminator - 1] = '\0'; // remove the trailing ','

    if(KS33500B_set_arbitrary_function_name(fd1, 1, "SF_Fun", arbFuncDataArray, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_function(fd1, 1, "ARB", error)) {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_arbitrary_function(fd1, 1, "SF_Fun", error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

//    if(KS33500B_set_voltage_low(fd1, 1, 0, error)){
//
//        printf("Error at set voltage amplitude: %s", error);
//        return KEYSIGHT_FAILURE;
//    }

    if(KS33500B_set_voltage_amplitude(fd1, 1, 5., error)){
        printf("Error at set voltage amplitude: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_voltage_offset(fd1, 1, 0, error)){
        printf("Error at set voltage offset: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_output(fd1, 1, 1, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    //**************************USBTMC1 initialise settings**************************

    if(KS33500B_set_voltage_amplitude(fd2, 1, 10, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_AM_function(fd2, 1 ,"SIN", error))//function is SIN,SQU,RAMP,NRAM,TRI,NOIS,PRBS,ARB
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_AM_state(fd2, 1 , 1, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_AM_source(fd2, 1 ,"EXT", error))//input source is INT/EXT/CH1/CH2
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33500B_set_AM_DSSC(fd2, 1 ,1, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33500B_set_AM_depth(fd2, 1 ,100, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_frequency(fd2, 1, 5.0E3, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_phase(fd2, 1, 0, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_output(fd2, 1, 1, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    //**************************USBTMC2 initialise settings**************************

    if(KS33500B_set_function(fd3, 1, "PULS", error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_frequency(fd3, 1, 20, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_voltage_amplitude(fd3,1,5,error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_voltage_offset(fd3,1,2.5,error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_pulse_width(fd3, 1, 10E-3, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_output(fd3, 1, 1, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

//    if (KS33500B_set_reset(fd1, error)) //sleep after reset (check function)
//    {
//        printf("Error: %s", error);
//        return KEYSIGHT_FAILURE;
//    }
//
//    if (KS33500B_set_reset(fd2, error)) //sleep after reset (check function)
//    {
//        printf("Error: %s", error);
//        return KEYSIGHT_FAILURE;
//    }

//**************************USBTMC3 initialise settings**************************

    if(KS33500B_set_voltage_amplitude(fd4, 1, 10, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_AM_function(fd4, 1 ,"SIN", error))//function is SIN,SQU,RAMP,NRAM,TRI,NOIS,PRBS,ARB
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_AM_state(fd4, 1 , 1, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_AM_source(fd4, 1 ,"EXT", error))//input source is INT/EXT/CH1/CH2
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33500B_set_AM_DSSC(fd4, 1 ,1, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33500B_set_AM_depth(fd4, 1 ,100, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_frequency(fd4, 1, 5.0E3, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_phase(fd4, 1, 90, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33500B_set_output(fd4, 1, 1, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    KS33500B_disconnect(fd1);
    KS33500B_disconnect(fd2);
    KS33500B_disconnect(fd3);
    KS33500B_disconnect(fd4);

    return 0;
}