#include <stdio.h>
#include <string.h>
#include <math.h>

#include "KS33X00X.h"

#include <time.h>
#include <unistd.h>

#define PI 3.141592653589
/************************************************************
 *  @details describes the function send to the audio ampli
 * @param t
 * @param freq of the sinus oscillation
 * @param t0
 * @param t1
 * @param order
 * @return
 */

int main(void)
{
    printf("\n +----------------------------------+");
    printf("\n |        Serial Port Read          |");
    printf("\n +----------------------------------+\n");
    const int fd = KS33X00X_connect("/dev/KS_WFG_05");
    if (fd == -1) {
        printf("\nERROR CONNECTING!\n");
        return -1;
    }
    char error[KEYSIGHT_MSG_SIZE];

    if (KS33X00X_set_reset(fd, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    sleep(5); //todo:this sleep function should really be replaced but it seems there is no way around it

    if(KS33X00X_set_ROsc_source(fd, (char*)"EXTernal", error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_output_load(fd, 1, 0, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_trigger_source(fd, 1, (char*)"EXTernal", error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_trigger_delay(fd, 1, 0, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_trigger_slope(fd, 1, (char*)"POS", error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_burst_mode(fd, 1, 1, error))//triggered mode
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_burst_count(fd, 1, 1, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_burst_state(fd, 1, 1, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_arbitrary_sample_rate(fd, 1, 50000, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    double sample_rate_out_SF1 = 0; //checks the set sample rate then passes this to the function to make
    if(KS33X00X_get_arbitrary_sample_rate(fd, 1, &sample_rate_out_SF1, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    int number_of_samples=2000;

    int length_SF1 = 0;

    const int samples_per_second_SF1 = (int)sample_rate_out_SF1;

    char arbFuncDataArray_SF1[(6 + 2) * number_of_samples];

    for (int s = 0; s < number_of_samples; ++s) {

        double t = (((double) s) / ((double)samples_per_second_SF1));

        double func_val_SF1 = SF_Damped(t,0.005, 0.0005,0.035,1);

        int adc_val = func_val_SF1 > 0 ? (int) floor(func_val_SF1 * 65535 / 2) : (int) ceil(func_val_SF1 * 65535 / 2);

        char cval[KEYSIGHT_MSG_SIZE];
        memset(cval, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(cval, "%d", adc_val);

        if (length_SF1 + strlen(cval) < sizeof(arbFuncDataArray_SF1)) {

            length_SF1 += sprintf(arbFuncDataArray_SF1 + length_SF1, "%s,", cval);
        }
    }

    size_t indexOfNullTerminator_SF1 = strlen(arbFuncDataArray_SF1);

    arbFuncDataArray_SF1[indexOfNullTerminator_SF1 - 1] = '\0'; // remove the trailing ','

    char wfg_func_name[KEYSIGHT_MSG_SIZE];
    if(KS33X00X_get_arbitrary_function_name(fd, 1, wfg_func_name, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    char waveform_name_1[KEYSIGHT_MSG_SIZE]= "SF1";

    if(wfg_func_name[3] != waveform_name_1[2]){ //todo: need to also add the flag here if the OBD is updated to check for changes in the waveform size

        if (KS33X00X_set_arbitrary_function_name(fd, 1, waveform_name_1, arbFuncDataArray_SF1, error)) {
            printf("Error: %s", error);
            return KEYSIGHT_FAILURE;
        }
    }

    if (KS33X00X_set_function(fd, 1, (char *) "ARB", error)) {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33X00X_set_arbitrary_function(fd, 1, waveform_name_1, error)) {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_voltage_amplitude(fd, 1, 5, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_voltage_offset(fd, 1, 0, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_output(fd, 1, 1, error)){
            printf("Error: %s", error);
            return KEYSIGHT_FAILURE;
    }

    /*if (KS33X00X_set_reset(fd, error))
    {
        printf("Error: %s", error);
        KS33X00X_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }

    char reply[KEYSIGHT_MSG_SIZE];

    if (KS33X00X_get_identify(fd, reply, error))
    {
        printf("Error: %s", error);
        KS33X00X_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("ID: %s", reply); //the printing here is without the carriage return as that is carried by the reply
    double frequency=0;
    for(int i=0; i<10; ++i)
    {
        if(KS33X00X_set_frequency(fd,1, 1000.+(float)i, error))
        {
            printf("Error: %s", error);
            return KEYSIGHT_FAILURE;
        }
        printf("Freq(%d): %f\n",i,frequency);

        if(KS33X00X_get_frequency(fd, 1, &frequency, error))
        {
            {
                printf("Error: %s", error);
                return KEYSIGHT_FAILURE;
            }
        }

    }

    if(KS33X00X_set_output_load(fd, 1, 0, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    double output_load = 0;
    if(KS33X00X_get_output_load(fd, 1, &output_load, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get output load: %0.6E\n", output_load);

    if(KS33X00X_set_phase(fd, 1, 90, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    double phase = 0;
    if(KS33X00X_get_phase(fd, 1, &phase, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get phase: %f\n", phase);

    if(KS33X00X_set_voltage_amplitude(fd, 1, 3, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    if(KS33X00X_set_voltage_high(fd, 1, 3, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    if(KS33X00X_set_voltage_low(fd, 1, -3, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    if(KS33X00X_set_voltage_offset(fd, 1, 2, error)){
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_output(fd, 1,1,error))
    {
        printf("Error: %s", error);
        KS33X00X_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_FAILURE;
    }
    double voltage = 0;
    if(KS33X00X_get_voltage_amplitude(fd, 1, &voltage, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get voltage: %f\n",voltage);

    double voltage_high = 0;
    if(KS33X00X_get_voltage_high(fd, 1, &voltage_high, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get voltage high: %f\n",voltage_high);

    double voltage_low = 0;
    if(KS33X00X_get_voltage_low(fd, 1, &voltage_low, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get voltage low: %f\n",voltage_low);

    double voltage_offset = 0;
    if(KS33X00X_get_voltage_offset(fd, 1, &voltage_offset, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get voltage offset: %f\n",voltage_offset);

    int state = 0;
    if(KS33X00X_get_output(fd, 1, &state, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    char on_off[KEYSIGHT_MSG_SIZE];
    if(state == 1){sprintf(on_off,"ON");}
    else{sprintf(on_off, "OFF");}
    printf("Get output: %s\n",on_off);

//    if(KS33X00X_set_burst_state(fd, 1, 1, error))
//    {
//        printf("Error: %s", error);
//        return KEYSIGHT_FAILURE;
//    }

    if(KS33X00X_set_burst_count(fd, 1, 1000, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_burst_period(fd, 1, 100, error)) {
        printf("Error: %s", error);

        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_burst_mode(fd, 1,1,error))
    {
        printf("Error: %s", error);
        KS33X00X_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_FAILURE;
    }

    int burst_state = 0;
    if(KS33X00X_get_burst_state(fd, 1,&burst_state,error))
    {
        printf("Error: %s", error);
        KS33X00X_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_FAILURE;
    }
    printf("Burst state: %d\n",burst_state);

    char burst_mode[KEYSIGHT_MSG_SIZE];
    if(KS33X00X_get_burst_mode(fd, 1,burst_mode,error))
    {
        printf("Error: %s", error);
        KS33X00X_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_FAILURE;
    }
    printf("Burst mode: %s",burst_mode);

    double burst_count = 0;
    if(KS33X00X_get_burst_count(fd, 1,&burst_count,error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Burst count: %f\n",burst_count);

    double burst_period = 0;
    if(KS33X00X_get_burst_period(fd, 1,&burst_period,error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Burst period: %f\n",burst_period);

    int length = 0;

    float freq = 1.;

    const int num_samples = 10000;

    const int samples_per_second = 5000;

    char arbFuncDataArray[(6 + 2) * num_samples];

    for (int s = 0; s < num_samples; ++s) {

        //float sine_val = sin(2.*3.141592653589*freq*t/samples_per_second);

        float t = (float) s / samples_per_second;

        float func_val = SF_Damped(t, 500., 0.5, 0.1, 1.);

        //printf("func_val(%f): %f\n",t,func_val);

        int adc_val = func_val > 0 ? (int) floor(func_val * 65535 / 2) : (int) ceil(func_val * 65535 / 2);

        char *cval = int_to_char_array(adc_val);


        if (length + strlen(cval) < sizeof(arbFuncDataArray)) {

            length += sprintf(arbFuncDataArray + length, "%s,", cval);

        }
    }

    size_t indexOfNullTerminator = strlen(arbFuncDataArray);

    arbFuncDataArray[indexOfNullTerminator - 1] = '\0'; // remove the trailing ','

    if(KS33X00X_set_arbitrary_function_name(fd, 1, "SF_Fun", arbFuncDataArray, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    char arb_func_name[KEYSIGHT_MSG_SIZE];
    if(KS33X00X_get_arbitrary_function_name(fd, 1, arb_func_name, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Arb function name: %s", arb_func_name);

    if(KS33X00X_set_function(fd, 1, "ARB", error)) {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    char function[KEYSIGHT_MSG_SIZE];
    if(KS33X00X_get_function(fd, 1, function, error)) {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get function: %s", function); //printing a char will carry the carriage return, printing the numbers don't carry the carriage return

    if(KS33X00X_set_arbitrary_function(fd, 1, "SF_Fun", error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_arbitrary_sample_rate(fd, 1, 1.0E3, error)) {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    double get_sample_rate = 0;
    if(KS33X00X_get_arbitrary_sample_rate(fd, 1, &get_sample_rate, error)) {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Arb sample rate: %f\n", get_sample_rate);

    if(KS33X00X_set_AM_function(fd, 1 ,"SIN", error))//function is SIN,SQU,RAMP,NRAM,TRI,NOIS,PRBS,ARB
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_reset(fd, error))
    {
        printf("Error: %s", error);
        KS33X00X_get_error_msg(fd, error);//this line only applies for function without error check
        printf("ERROR DEVICE CHECK: %s", error);//this line only applies for function without error check
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_AM_state(fd, 1 , 1, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_AM_frequency(fd, 1 , 5, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if(KS33X00X_set_AM_source(fd, 1 ,"INT", error))//input source is INT/EXT/CH1/CH2
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33X00X_set_AM_DSSC(fd, 1 ,1, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33X00X_set_AM_depth(fd, 1 ,50, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    double percentage = 0;
    if (KS33X00X_get_AM_depth(fd, 1 ,&percentage, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get AM Depth [1]: %f\n", percentage);

    int AM_state = 0;
    if (KS33X00X_get_AM_state(fd, 1 ,&AM_state, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get AM State [1]: %d\n", AM_state);

    int AM_DSSC_state = 0;
    if (KS33X00X_get_AM_DSSC(fd, 1 ,&AM_DSSC_state, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get AM DSSC State [1]: %d\n", AM_DSSC_state);

    double AM_frequency = 0;
    if (KS33X00X_get_AM_frequency(fd, 1 ,&AM_frequency, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get AM frequency [1]: %f\n", AM_frequency);

    char AM_function[KEYSIGHT_MSG_SIZE];
    if (KS33X00X_get_AM_function(fd, 1 ,AM_function, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get AM function [1]: %s", AM_function);

    char AM_source[KEYSIGHT_MSG_SIZE];
    if (KS33X00X_get_AM_source(fd, 1 ,AM_source, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get AM source [1]: %s", AM_source);

    if (KS33X00X_set_trigger(fd, 1 , error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    if (KS33X00X_set_trigger_count(fd, 1 , 5, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    int trigger_count = 0;
    if (KS33X00X_get_trigger_count(fd, 1 ,&trigger_count, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get trigger count [1]: %d\n", trigger_count);

    if (KS33X00X_set_trigger_delay(fd, 1 , 5, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    double trigger_delay = 0;
    if (KS33X00X_get_trigger_delay(fd, 1 ,&trigger_delay, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get trigger delay [1]: %f\n", trigger_delay);

    if (KS33X00X_set_trigger_slope(fd, 1 , "POS", error)) //{POSitive|NEGative}
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    char trigger_slope[KEYSIGHT_MSG_SIZE];
    if (KS33X00X_get_trigger_slope(fd, 1 ,trigger_slope, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get trigger slope [1]: %s", trigger_slope);

    if (KS33X00X_set_trigger_source(fd, 1 , "EXTernal", error)) //{IMMediate|EXTernal|TIMer|BUS}
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    char trigger_source[KEYSIGHT_MSG_SIZE];
    if (KS33X00X_get_trigger_source(fd, 1 ,trigger_source, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get trigger source [1]: %s", trigger_source);

    if (KS33X00X_set_trigger_timer(fd, 1 , 5, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }

    double trigger_timer;
    if (KS33X00X_get_trigger_timer(fd, 1 ,&trigger_timer, error))
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }
    printf("Get trigger timer [1]: %f\n", trigger_timer);

    if (KS33X00X_set_reset(fd, error)) //sleep after reset (check function)
    {
        printf("Error: %s", error);
        return KEYSIGHT_FAILURE;
    }*/
    KS33X00X_disconnect(fd);

    return 0;
}

//to check for timing of functions:
//clock_t time_req;
//time_req = clock();

//time_req = clock() - time_req;
//printf("mseconds: %f\n", (float)time_req*1000/CLOCKS_PER_SEC);


