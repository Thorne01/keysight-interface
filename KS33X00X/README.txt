Headers, functions and example for running the waveform generator KS33X00X.

The communication to the waveform generator is done via USB TMC. This does not use terminos in order to communicate. However, this means there is potentially no flow control on the write and read functions. According to this thread: https://community.keysight.com/thread/19408, between command and a query, a delay of about 1 second is required. It was found that sending OPC check and SYSTem:ERRor? queries did not cause problems. Therefore, the delay is added in the read function and only for the main function command.

+410/420 errors can occur if the delay between commands or requests is not long enough. User should be careful when implimenting.

*OPC? Check is performed within the function after a command has been sent. This checks the device has read it, if 0 is returned then error is produced.

Note running the functions out of order given in the example could produce errors, this can be checks via error handling method. If error is returned then this will be the same as that given in the Keysight KS33X00X documentation. 

In every function thats used it will always pass by reference error message which can be implimented however the user needs. All functions that require user input have the error check function built in, either custom error return or from the standard keysight documentation.

All getters in the following functions require a channel to be stated, either 1/2.

List of functions and purpose:

*****Communication functions*****

const int KS33X00X_connect(const char* port_id);
int KS33X00X_disconnect(const int);

int KS33X00X_write_msg(const int serial_port, char* msg, char* error_msg);
int KS33X00X_read_msg(const int serial_port, char* reply, char* error_msg);

*****Check/initialising functions*****

int KS33X00X_set_reset(const int serial_port, char* error); //reset to default 
int KS33X00X_get_error_msg(const int serial_port, char* reply); //error message query
int KS33X00X_get_identify(const int serial_port, char* reply, char* error); //get device ID
int KS33X00X_abort(const int serial_port, char* reply); //abort to stop any keysight sent command currently running
int KS33X00X_OPC(const int serial_port, char* reply); //confirm command function, returns 1 if received, 0 for nothing

*****General functions*****

int KS33X00X_set_function(const int serial_port, int source, char* function, char* error); //sets a function on given channel: SIN,SQU,RAMP,NRAM,TRI,NOIS,PRBS,ARB,PULS

int KS33X00X_set_output(const int serial_port, int source, int state, char* error); //set ouput on given channel, 1/0.
int KS33X00X_set_output_load(const int serial_port, int source, int load, char* error); //set output load on given channel, 0 = infinity
int KS33X00X_get_function(const int serial_port, int source, char* function, char* error);
int KS33X00X_get_output(const int serial_port, int source, int* state, char* error);
int KS33X00X_get_output_load(const int serial_port, int source, double* load, char* error); //getters for all above

*****Set voltage functions*****

int KS33X00X_set_voltage_amplitude(const int serial_port, int source, double v_amp, char* error); //set V_pp for given channel
int KS33X00X_set_voltage_high(const int serial_port, int source, double v_high, char* error); 
int KS33X00X_set_voltage_low(const int serial_port, int source, double v_low, char* error); //set waveforms high and low voltage for given channel

int KS33X00X_set_voltage_offset(const int serial_port, int source, double v_offset, char* error); //set voltage offset for given channel

int KS33X00X_get_voltage_amplitude(const int serial_port, int source, double* voltage, char* error);
int KS33X00X_get_voltage_high(const int serial_port, int source, double* voltage_high, char* error);
int KS33X00X_get_voltage_low(const int serial_port, int source, double* voltage_low, char* error);
int KS33X00X_get_voltage_offset(const int serial_port, int source, double* offset, char* error); //getters for all above

*****Set burst functions*****

int KS33X00X_set_burst_state(const int serial_port, int source, int on_off, char* error); //set state of burst, 1/0 for given channel
int KS33X00X_set_burst_mode(const int serial_port, int source, int mode, char* error); //set mode for burst, triggered (>=1) or gated (anything else) for given channel.

int KS33X00X_set_burst_count(const int serial_port, int source, double count, char* error); //sets the burst count, stops after count reached for given channel

int KS33X00X_set_burst_period(const int serial_port, int source, double seconds, char* error); //sets the burst mode to stop after given time for set channel

int KS33X00X_get_burst_state(const int serial_port, int source, int* on_off, char* error);
int KS33X00X_get_burst_mode(const int serial_port, int source, char* mode, char* error);
int KS33X00X_get_burst_count(const int serial_port, int source, double* count, char* error);
int KS33X00X_get_burst_period(const int serial_port, int source, double* seconds, char* error); //getters for all above

*****Set frequency/phase functions*****

int KS33X00X_set_frequency(const int serial_port, int source, double frequency, char* error); //sets frequency for given channel
int KS33X00X_set_phase(const int serial_port, int source, double frequency, char* error); //sets phase for given channel

int KS33X00X_get_frequency(const int serial_port, int source, double* frequency, char* error);
int KS33X00X_get_phase(const int serial_port, int source, double* phase, char* error); //getters for above

*****Set arbitrary parameters*****

int KS33X00X_set_arbitrary_function(const int serial_port, int source, char* function, char* error); //sets the device to use the function given from the arbitrary function name function for given channel. The char passed is the name of the function.

int KS33X00X_set_arbitrary_sample_rate(const int serial_port, int source, double sample_rate, char* error); //sets the sample rate of the given waveform for a set channel.

int KS33X00X_set_arbitrary_function_name(const int serial_port, int source, const char* name, const char* data, char* error); //programmes an arbitrary waveform into the device with a given name passed as the const char*. The data is in the form: floating point values
sent into waveform volatile memory as a list of comma separated values.

*****Set modulation parameters*****

int KS33X00X_set_AM_depth(const int serial_port, int source, double percentage, char* error); //sets internal modulation depth ("percent modulation") in percent, for given channel.

int KS33X00X_set_AM_source(const int serial_port, int output_source, char* input_source, char* error);//sets the source, either INTernal/ EXTernal, CH1, CH2 for given channel.

int KS33X00X_set_AM_function(const int serial_port, int source, char* function, char* error);//sets function to be modulated, for given channel.

int KS33X00X_set_AM_frequency(const int serial_port, int source, double frequency, char* error);//sets modulation frequency for given channel.

int KS33X00X_set_AM_DSSC(const int serial_port, int source, int state, char* error);//sets Double Sideband Suppressed Carrier for given channel.

int KS33X00X_set_AM_state(const int serial_port, int source, int state, char* error);//this will turn on the modulation for given channel.

int KS33X00X_get_AM_depth(const int serial_port, int source, double* percentage, char* error);
int KS33X00X_get_AM_state(const int serial_port, int source, int* state, char* error);
int KS33X00X_get_AM_DSSC(const int serial_port, int source, int* state, char* error);
int KS33X00X_get_AM_frequency(const int serial_port, int source, double* frequency, char* error);
int KS33X00X_get_AM_function(const int serial_port, int source, char* AM_function, char* error);
int KS33X00X_get_AM_source(const int serial_port, int output_source, char* input_source, char* error); //getters for above

*****Set trigger parameters*****

int KS33X00X_set_trigger(const int serial_port, int source, char* error); //PC controlled trigger, this will have a delay of 750ms to write this command due to timing issue stated above.

int KS33X00X_set_trigger_count(const int serial_port, int source, int count, char* error); //set the trigger count
int KS33X00X_set_trigger_delay(const int serial_port, int source, double seconds, char* error); //set a trigger delay of 0 to 1000 s, in resolution of 4 ns; default 0

int KS33X00X_set_trigger_slope(const int serial_port, int source, char* slope, char* error); //set the trigger to be positive or negative
int KS33X00X_set_trigger_source(const int serial_port, int source, char* trigger_input, char* error); //sets the source of the trigger: immediate or timed internal trigger, an external hardware trigger from the rear-panel Ext Trig connector, or a software (bus) trigger.

int KS33X00X_set_trigger_timer(const int serial_port, int source, double trigger_timer, char* error); //sets a timer on the trigger, 1 μs to 8,000 s.
int KS33X00X_set_trigger_level(const int serial_port, int source, double voltage, char* error); //sets the trigger level, 0.9 to 3.8 V, only for KS33600A versions.

int KS33X00X_get_trigger_count(const int serial_port, int source, int* count, char* error);
int KS33X00X_get_trigger_delay(const int serial_port, int source, double* seconds, char* error);
int KS33X00X_get_trigger_slope(const int serial_port, int source, char* slope, char* error);
int KS33X00X_get_trigger_source(const int serial_port, int source, char* trigger_input, char* error);
int KS33X00X_get_trigger_timer(const int serial_port, int source, double* trigger_timer, char* error); //getters for above
int KS33X00X_get_trigger_level(const int serial_port, int source, double* voltage, char* error); //only for KS33600A versions

int KS33X00X_set_ROsc_auto(const int serial_port, int on_off, char* error); //disables or enables automatic selection of reference oscillator signal source.
int KS33X00X_set_ROsc_source(const int serial_port, char* source, char* error); //selects internal or external reference oscillator source.

int KS33X00X_get_ROsc_auto(const int serial_port, char* state, char* error);
int KS33X00X_get_ROsc_source(const int serial_port, char* source, char* error);//gets the information from the set functions above

int KS33X00X_set_custom_command(const int serial_port, char* string, char* error); //function where a custom string of the form "SOURce1:FREQency 10", for example to set channel one frequency to 10 Hz. This only works for set commands.
int KS33X00X_get_custom_command(const int serial_port, char* string, char* reply, char* error); //function with custom command with strings of the form "SOURce1:FREQency?" for example. This will reply with a char* in the function. This only works for get commands.




