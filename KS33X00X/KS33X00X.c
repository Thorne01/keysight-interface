#include <linux/usb/tmc.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include "KS33X00X.h"
#include <math.h>

double SF_Damped(double t, double t0, double t1, double t2, double order){

    if(t < t0) {
        return 1;
    }
    else{
        if(t < t2)
        {
            return pow((t0 - t1) / (t - t1), order);
        }
        else{
            return 0;

        }
    }
}

const int KS33X00X_connect(const char* port_id)
{
    struct usbtmc_attribute attr;
    int serial_port=open(port_id, O_RDWR | O_NOCTTY  | O_SYNC);
    attr.attribute=1;
    attr.value=1; // using read, not fread
    ioctl(serial_port,_IO(USBTMC_IOC_NR,5),&attr);

    return serial_port;
}

int KS33X00X_disconnect(const int serial_port)
{
    return close(serial_port);
}

int KS33X00X_write_msg(const int serial_port, char* msg, char* error_msg)
{
    //printf("SENDING: %s",msg);
    int nbytes_written = write(serial_port, msg, strlen(msg));
    usleep(750000);
    if(nbytes_written != strlen(msg))
    {sprintf(error_msg,"WRITE ERROR!");return KEYSIGHT_COM_ERR;} //error check
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_write_msg_fast(const int serial_port, char* msg, char* error_msg)
{
    int nbytes_written = write(serial_port, msg, strlen(msg));
    if(nbytes_written != strlen(msg))
    {sprintf(error_msg,"WRITE ERROR!");return KEYSIGHT_COM_ERR;} //error check
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_read_msg(const int serial_port, char* reply, char* error_msg)
{
    memset(reply, '\0', sizeof(char)*KEYSIGHT_MSG_SIZE);
    int err = read(serial_port, reply, sizeof(char)*KEYSIGHT_MSG_SIZE); //reads back the number of bytes
    //printf("READING: %s", reply);
    if(err <= 0)
    {
        sprintf(error_msg, "READ ERROR!");
        return KEYSIGHT_COM_ERR;
    }
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_abort(const int serial_port, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    memset(error,'\0', sizeof(char)*KEYSIGHT_MSG_SIZE);
    if(KS33X00X_write_msg(serial_port, "ABORt\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_get_error_msg(const int serial_port, char* reply)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char lreply[KEYSIGHT_MSG_SIZE];
    memset(reply,'\0', sizeof(char)*KEYSIGHT_MSG_SIZE);
    write(serial_port, "SYSTem:ERRor?\n", 14);
    // if(KS33X00X_write_msg(serial_port, "SYSTem:ERRor?\r\n", error_msg)){sprintf(reply,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KS33X00X_read_msg(serial_port,lreply,error_msg))
    {
        sprintf(reply,"%s",error_msg);
        return KEYSIGHT_COM_ERR;
    }
    int size = strlen(lreply);//length of the string to remove the CF from any returned message
    snprintf(reply, size,"%s",lreply);//removes CF
    write(serial_port, "*CLS\n", 6);
    //if(KS33X00X_write_msg(serial_port, "*CLS\r\n", error_msg)){sprintf(reply,"ERROR MESSAGE FLUSH: %s\n",error_msg);return KEYSIGHT_COM_ERR;}
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_OPC(const int serial_port, char* reply)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    memset(reply,'\0', sizeof(char)*KEYSIGHT_MSG_SIZE);
    write(serial_port, "*OPC?\n", 5);
    if(KS33X00X_read_msg(serial_port,reply,error_msg)){sprintf(reply,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_set_reset(int serial_port, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KS33X00X_write_msg(serial_port, "*RST\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE*sizeof(char));
    if(KS33X00X_write_msg(serial_port, "*CLS\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;};
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_get_identify(const int serial_port, char* reply, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(reply,'\0', sizeof(char)*KEYSIGHT_MSG_SIZE);
    if(KS33X00X_write_msg(serial_port, "*IDN?\r\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KS33X00X_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_set_function(const int serial_port, int source, char* function, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	if(source == 1 || source == 2)
	{
        char msg[KEYSIGHT_MSG_SIZE];
        char error_msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));

        sprintf(msg, "SOURce%d:FUNCtion %s\n",source, function);
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if(KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if(KS33X00X_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET FUNCTION COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] == '0'){return KEYSIGHT_SUCCESS;}
        else{sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
	}
	else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_function(const int serial_port, int source, char* function, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char msg[KEYSIGHT_MSG_SIZE];
        char error_msg[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(function,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));

        sprintf(msg, "SOURce%d:FUNCtion?\n",source);
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if (KS33X00X_read_msg(serial_port, function, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_voltage_amplitude(const int serial_port, int source, double v_amp, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0',KEYSIGHT_MSG_SIZE*sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE*sizeof(char));

        sprintf(msg, "SOURce%d:VOLTage %0.6E\n",source, v_amp);
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != 49){sprintf(error, "SET VOLTAGE HIGH COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}

        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_voltage_high(const int serial_port, int source, double v_high, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	if(source == 1 || source == 2)
	{
		char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0',KEYSIGHT_MSG_SIZE*sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE*sizeof(char));

        sprintf(msg, "SOURce%d:VOLTage:HIGH %0.6E\n",source, v_high);
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != 49){sprintf(error, "SET VOLTAGE HIGH COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}

        return KEYSIGHT_SUCCESS;
	}
	else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_voltage_low(const int serial_port, int source, double v_low, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    if (source == 1 || source == 2) {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));

        sprintf(msg, "SOURce%d:VOLTage:LOW %0.6E\n", source, v_low);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if (reply[0] != 49) {
            sprintf(error, "SET VOLTAGE LOW COMMAND NOT EXECUTED!");
            return KEYSIGHT_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }

        return KEYSIGHT_SUCCESS;
    }
}

int KS33X00X_set_voltage_offset(const int serial_port, int source, double v_offset, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    if (source == 1 || source == 2) {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));

        sprintf(msg, "SOURce%d:VOLTage:OFFset %0.6E\n", source, v_offset);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if (reply[0] != 49) {
            sprintf(error, "SET VOLTAGE OFFSET COMMAND NOT EXECUTED!");
            return KEYSIGHT_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
}

int KS33X00X_get_frequency(int serial_port, int source, double* frequency, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];

        char reply[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *
                            sizeof(char));//memset required at the start of all the functions in order to remove crap at end of the read

        sprintf(msg, "SOURce%d:FREQuency?\n", source);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *frequency = strtod(reply, NULL); //cannot add *OPC? check here as there is nothing to check
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_frequency(const int serial_port, int source, double frequency, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char msg[KEYSIGHT_MSG_SIZE];

        char error_msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *
                            sizeof(char));//memset required at the start of all the functions in order to remove crap at end of the read

        sprintf(msg, "SOURce%d:FREQuency %f\n", source, frequency);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if (reply[0] != 49) {
            sprintf(error, "SET FREQUENCY COMMAND NOT EXECUTED!");
            return KEYSIGHT_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}
int KS33X00X_set_output(const int serial_port, int source, int on_off, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	if(source == 1 || source == 2)
	{
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char state[KEYSIGHT_MSG_SIZE];
        if(on_off > 1 || on_off < 0){sprintf(error, "SET OUTPUT ON/OFF COMMAND OUT OF RANGE!");return KEYSIGHT_ERR;}
        if(on_off == 0){sprintf(state, "OFF");}
        else{sprintf(state, "ON");}
        memset(msg,'\0', sizeof(msg));
        sprintf(msg, "OUTPut%d %s\n", source, state);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        char reply[KEYSIGHT_MSG_SIZE];
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET OUTPUT COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
	}
	else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_output_load(const int serial_port, int source, int load, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0', sizeof(msg));
        memset(reply,'\0', sizeof(msg));

        if(load == 0)
        {
            sprintf(msg, "OUTPut%d:LOAD INFinity\n",source);
        }
        else
        {
            sprintf(msg, "OUTPut%d:LOAD %d\n", source, load);
        }
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != 49){sprintf(error, "SET OUTPUT LOAD COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_output_load(const int serial_port, int source, double* load, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0', sizeof(msg));
        memset(reply,'\0', sizeof(msg));

        sprintf(msg, "OUTPut%d:LOAD?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *load = strtod(reply, NULL); //cannot add *OPC? check here as there is nothing to check
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_phase(const int serial_port, int source, double phase, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];

        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));

        sprintf(msg, "SOURce%d:PHASe %0.6E\n",source, phase);
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != 49){sprintf(error, "SET PHASE COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_zero_phase(const int serial_port, int source, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];

        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));

        sprintf(msg, "SOURce%d:PHASe:REFerence\n",source);
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != 49){sprintf(error, "SET PHASE COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_phase_sync(const int serial_port, char* error)
{
    char reply[KEYSIGHT_MSG_SIZE];

    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));

    if(KS33X00X_write_msg(serial_port, "PHASe:SYNChronize\n", error)){return KEYSIGHT_COM_ERR;}
    KS33X00X_OPC(serial_port, reply);
    if(reply[0] != 49){sprintf(error, "SET PHASE SYNC COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
    if(KS33X00X_get_error_msg(serial_port, error)){return KEYSIGHT_COM_ERR;}
    if(error[1] != '0'){return KEYSIGHT_ERR;}
    return KEYSIGHT_SUCCESS;
}


int KS33X00X_get_phase(const int serial_port, int source, double* phase, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];

        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));

        sprintf(msg, "SOURce%d:PHASe?\n",source);
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *phase = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_burst_mode(const int serial_port, int source, int mode, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	if(source == 1 || source == 2)
	{
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	    if (mode >= 1)
		{
            sprintf(msg, "SOURce%d:BURSt:MODE TRIGgered\n", source);
        }
	    else{sprintf(msg, "SOURce%d:BURSt:MODE GATed\n",source);}

	    if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET BURST MODE COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
	}
	else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_burst_mode(const int serial_port, int source, char* mode, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(mode,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:BURSt:MODE?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, mode, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}


int KS33X00X_get_voltage_amplitude(int serial_port, int source, double* voltage, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];

        char reply[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *
                            sizeof(char));//memset required at the start of all the functions in order to remove crap at end of the read
        sprintf(msg, "SOURce%d:VOLTage?\n", source);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *voltage = strtod(reply, NULL); //cannot add *OPC? check here as there is nothing to check
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_voltage_high(int serial_port, int source, double* voltage_high, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];

        char reply[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *
                            sizeof(char));//memset required at the start of all the functions in order to remove crap at end of the read
        sprintf(msg, "SOURce%d:VOLTage:HIGH?\n", source);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *voltage_high = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_voltage_low(int serial_port, int source, double* voltage_low, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];

        char reply[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *
                            sizeof(char));//memset required at the start of all the functions in order to remove crap at end of the read
        sprintf(msg, "SOURce%d:VOLTage:LOW?\n", source);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *voltage_low = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_voltage_offset(int serial_port, int source, double* offset, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];

        char reply[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *
                            sizeof(char));//memset required at the start of all the functions in order to remove crap at end of the read
        sprintf(msg, "SOURce%d:VOLTage:OFFSet?\n", source);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *offset = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_output(int serial_port, int source, int* state, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(state, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "OUTPut%d?\n", source);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        sscanf(reply,"%d",state);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_burst_count(const int serial_port, int source, double count, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	if(source == 1 || source == 2)
	{
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
		if(count == 0)
		{
			sprintf(msg, "SOURce%d:BURSt:NCYCles INFinity\n",source);
		}
		else{sprintf(msg, "SOURce%d:BURSt:NCYCles %0.6E\n",source, count);}

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET BURST COUNT COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
	}
	else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_burst_count(const int serial_port, int source, double* count, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:BURSt:NCYCles?\n",source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *count = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_burst_state(const int serial_port, int source, int on_off, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char state[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        if(on_off > 1 || on_off < 0){sprintf(error, "SET BURST ON/OFF COMMAND OUT OF RANGE!");return KEYSIGHT_ERR;}
        if(on_off == 0){sprintf(state, "OFF");}
        else{sprintf(state, "ON");}
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:BURSt:STATe %s\n", source, state);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET BURST STATE COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
	else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_burst_state(const int serial_port, int source, int* on_off, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];

        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:BURSt:STATe?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        sscanf(reply,"%d",on_off);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_burst_period(const int serial_port, int source, double seconds, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:BURSt:INTernal:PERiod %0.6E\n", source, seconds);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}

        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET BURST PERIOD COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_burst_period(const int serial_port, int source, double* seconds, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:BURSt:INTernal:PERiod?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *seconds = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_arbitrary_function(const int serial_port, int source, char* function, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	if(source == 1 || source == 2)
	{
		char error_msg[KEYSIGHT_MSG_SIZE];
		char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
		memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
		sprintf(msg, "SOURce%d:FUNCtion:ARBitrary %s\n",source, function);
		if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}

        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
		if(reply[0] != '1'){sprintf(error, "SET ARBITRARY FUNCTION COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
		if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
		if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
		return KEYSIGHT_SUCCESS;
	}
	else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_arbitrary_sample_rate(const int serial_port, int source, double sample_rate, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if (source == 1 || source == 2) {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];

        memset(msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:FUNCtion:ARBitrary:SRAT %0.6e\n", source, sample_rate);

        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if (reply[0] != '1') {
            sprintf(error, "SET ARBITRARY SAMPLE RATE COMMAND NOT EXECUTED!");
            return KEYSIGHT_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    } else {
        sprintf(error, "SET CHANNEL OUT OF RANGE!");
        return KEYSIGHT_ERR;
    }
}

int KS33X00X_get_arbitrary_sample_rate(const int serial_port, int source, double* sample_rate, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if (source == 1 || source == 2) {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];

        memset(msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:FUNCtion:ARBitrary:SRAT?\n", source);

        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *sample_rate = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    } else {
        sprintf(error, "SET CHANNEL OUT OF RANGE!");
        return KEYSIGHT_ERR;
    }
}

int KS33X00X_set_arbitrary_function_name(const int serial_port, int source, const char* name, const char* data, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if (source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        char msg[strlen(data)+KEYSIGHT_MSG_SIZE];
        memset(reply, '\0', sizeof(reply));
        memset(msg, '\0', (strlen(data)+KEYSIGHT_MSG_SIZE)*sizeof(char));
        sprintf(msg, "SOURce%d:DATA:ARBitrary:DAC %s, %s\n", source, name, data);

        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if (reply[0] != '1') {
            sprintf(error, "SET ARBITRARY FUNCTION NAME COMMAND NOT EXECUTED!");
            return KEYSIGHT_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else {
        sprintf(error, "SET CHANNEL OUT OF RANGE!");
        return KEYSIGHT_ERR;
    }
}

int KS33X00X_get_arbitrary_function_name(const int serial_port, int source, char* name, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if (source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(name, '\0',  KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:FUNCtion:ARBitrary?\n", source);

        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if(KS33X00X_read_msg(serial_port, name, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_AM_depth(const int serial_port, int source, double percentage, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:DEPTh %f\n",source, percentage);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}

        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET AM DEPTH COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_AM_DSSC(const int serial_port, int source, int state, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:DSSC %d\n",source, state);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET AM DEPTH COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_AM_state(const int serial_port, int source, int state, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:STATe %d\n",source, state);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET AM DEPTH COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_AM_source(const int serial_port, int output_source, char* input_source, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(output_source == 1 || output_source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:SOURce %s\n",output_source, input_source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET AM DEPTH COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}
///
/// \param serial_port
/// \param source
/// \param function
/// \param error
/// \return
int KS33X00X_set_AM_function(const int serial_port, int source, char* function, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:INTernal:FUNCtion %s\n",source, function);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET AM DEPTH COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}
/// sets the AM frequency
/// \param serial_port file descriptor for the serial connnection
/// \param source
/// \param frequency
/// \param error
/// \return
int KS33X00X_set_AM_frequency(const int serial_port, int source, double frequency, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:INTernal:FREQuency %0.6E\n",source, frequency);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        //if (KS33X00X_read_msg(serial_port, reply, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET AM DEPTH COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_AM_depth(int serial_port, int source, double* percentage, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];

        char reply[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:DEPTh?\n", source);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *percentage = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_AM_source(const int serial_port, int output_source, char* input_source, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(output_source == 1 || output_source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(input_source, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:SOURce?\n", output_source);
        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_read_msg(serial_port, input_source, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_AM_function(const int serial_port, int source, char* AM_function, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(AM_function,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:INTernal:FUNCtion?\n",source);
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if (KS33X00X_read_msg(serial_port, AM_function, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_AM_frequency(const int serial_port, int source, double* frequency, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:INTernal:FREQuency?\n",source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *frequency = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_AM_DSSC(const int serial_port, int source, int* state, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:DSSC?\n",source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        sscanf(reply,"%d",state);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_AM_state(const int serial_port, int source, int* state, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:AM:STATe?\n",source);
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        sscanf(reply,"%d",state);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_trigger(const int serial_port, int source, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d\n",source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET TRIGGER COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_trigger_count(const int serial_port, int source, int count, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:COUNt %d\n", source, count);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET TRIGGER COUNT COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_trigger_count(const int serial_port, int source, int* count, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:COUNt?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        sscanf(reply,"%d",count);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_trigger_delay(const int serial_port, int source, double seconds, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:DELay %0.6E\n", source, seconds);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET TRIGGER DELAY COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_trigger_delay(const int serial_port, int source, double* seconds, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:DELay?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *seconds = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_trigger_slope(const int serial_port, int source, char* slope, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:SLOPe %s\n", source, slope);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET TRIGGER SLOPE COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_trigger_slope(const int serial_port, int source, char* slope, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(slope,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:SLOPe?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, slope, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_trigger_source(const int serial_port, int source, char* trigger_input, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:SOURce %s\n", source, trigger_input);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET TRIGGER SOURCE COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_trigger_source(const int serial_port, int source, char* trigger_input, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        memset(trigger_input,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:SOURce?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, trigger_input, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_trigger_timer(const int serial_port, int source, double trigger_timer, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:TIMer %0.6E\n", source, trigger_timer);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET TRIGGER TIMER COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_trigger_timer(const int serial_port, int source, double* trigger_timer, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:DELay?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *trigger_timer = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_trigger_level(const int serial_port, int source, double voltage, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:LEVel %0.3E\n", source, voltage);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET TRIGGER LEVEL COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_trigger_level(const int serial_port, int source, double* voltage, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "TRIGger%d:LEVel?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *voltage = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_pulse_width(const int serial_port, int source, double width_sec, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(error_msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:FUNCtion:PULSe:WIDTh %0.9E\n",source,width_sec);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "SET TRIGGER COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_get_pulse_width(const int serial_port, int source, double* width_sec, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:FUNCtion:PULSe:WIDTh?\n", source);

        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KS33X00X_read_msg(serial_port, reply, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }
        *width_sec = strtod(reply, NULL);
        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_load_arbitrary_function(const int serial_port, int source, char* memory_location_local_or_usb, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(reply,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "MMEMory:LOAD:DATA%d %s\n",source, memory_location_local_or_usb); //format for char*: "Int:\Builtin\HAVERSINE.arb" or from USB: "USB:\arbMonday"
        if(KS33X00X_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        KS33X00X_OPC(serial_port, reply);
        if(reply[0] != '1'){sprintf(error, "LOAD ARBITRARY FUNCTION COMMAND NOT EXECUTED!"); return KEYSIGHT_ERR;}
        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_ROsc_source(const int serial_port, char* source, char* error) {
    memset(error, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    char error_msg[KEYSIGHT_MSG_SIZE];
    char msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "ROSCillator:SOURce %s\n", source);

    if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    KS33X00X_OPC(serial_port, reply);
    if (reply[0] != '1') {
        sprintf(error, "SET ROSC COMMAND NOT EXECUTED!");
        return KEYSIGHT_ERR;
    }
    if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] != '0') {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
    return KEYSIGHT_SUCCESS;

}

int KS33X00X_get_ROsc_source(const int serial_port, char* source, char* error) {
    memset(error, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    char error_msg[KEYSIGHT_MSG_SIZE];
    char msg[KEYSIGHT_MSG_SIZE];
    memset(source, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "ROSCillator:SOURce?\n");

    if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KS33X00X_read_msg(serial_port, source, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] != '0') {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_set_ROsc_auto(const int serial_port, int on_off, char* error) {

    char error_msg[KEYSIGHT_MSG_SIZE];
    char msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    char state[KEYSIGHT_MSG_SIZE];

    memset(error, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));

    if(on_off > 1 || on_off < 0){sprintf(error, "SET ROSC AUTO ON/OFF COMMAND OUT OF RANGE!");return KEYSIGHT_ERR;}
    if(on_off == 0){sprintf(state, "OFF");}
    else{sprintf(state, "ON");}

    sprintf(msg, "ROSCillator:SOURce:AUTO %s\n", state);

    if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    KS33X00X_OPC(serial_port, reply);
    if (reply[0] != '1') {
        sprintf(error, "SET ROSC AUTO COMMAND NOT EXECUTED!");
        return KEYSIGHT_ERR;
    }
    if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] != '0') {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
    return KEYSIGHT_SUCCESS;

}

int KS33X00X_get_ROsc_auto(const int serial_port, char* state, char* error) {
    memset(error, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    char error_msg[KEYSIGHT_MSG_SIZE];
    char msg[KEYSIGHT_MSG_SIZE];
    memset(state, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "ROSCillator:SOURce:AUTO?\n");

    if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KS33X00X_read_msg(serial_port, state, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] != '0') {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_set_custom_command(const int serial_port, char* string, char* error) {

    char error_msg[KEYSIGHT_MSG_SIZE];
    char msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];

    memset(error, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));

    sprintf(msg, "%s\n", string);

    if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }

    if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] != '0') {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_get_custom_command(const int serial_port, char* string, char* reply, char* error) {

    char error_msg[KEYSIGHT_MSG_SIZE];
    char msg[KEYSIGHT_MSG_SIZE];

    memset(error, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));

    sprintf(msg, "%s\n", string);

    if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }

    if (KS33X00X_read_msg(serial_port, reply, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }

    if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] != '0') {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_clear_volatile_memory(const int serial_port, int source, char* error) {

    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2) {
        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];

        memset(error, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));

        sprintf(msg, "SOURce%d:DATA:VOLatile:CLEar\n", source);

        if (KS33X00X_write_msg(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }

        if (KS33X00X_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
    }
    return KEYSIGHT_SUCCESS;
}

int KS33X00X_set_sf_parameters(const int serial_port, int source, double frequency, double amplitude, double phase, int output_state, int AM_state, char* error)
{
    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(source == 1 || source == 2)
    {
        char msg[KEYSIGHT_MSG_SIZE];
        char error_msg[KEYSIGHT_MSG_SIZE];
        char reply[KEYSIGHT_MSG_SIZE];
        memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
        memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));

        //sprintf(msg, "SOURce%d:FREQuency %f;*WAI;SOURce%d:VOLTage %0.6E;*WAI;SOURce%d:PHASe %0.6E;*WAI\n", source, frequency, source, amplitude, source, phase);

        sprintf(msg, "SOURce%d:FREQuency %f;*WAI\n", source, frequency);
        if (KS33X00X_write_msg_fast(serial_port, msg, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}

        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }

        sprintf(msg, "SOURce%d:VOLTage %0.6E;*WAI\n", source, amplitude);
        if (KS33X00X_write_msg_fast(serial_port, msg, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}

        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }

        sprintf(msg, "SOURce%d:PHASe %0.6E;*WAI\n", source, phase);
        if (KS33X00X_write_msg_fast(serial_port, msg, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}

        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }

	sprintf(msg, "OUTPut%d %d;*WAI\n", source, output_state);
        if (KS33X00X_write_msg_fast(serial_port, msg, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}

        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }

	sprintf(msg, "SOURce%d:AM:STATe %d;*WAI\n", source, AM_state);
        if (KS33X00X_write_msg_fast(serial_port, msg, error_msg)){sprintf(error, "%s", error_msg);return KEYSIGHT_COM_ERR;}

        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }
        return KEYSIGHT_SUCCESS;
    }
    else{sprintf(error, "SET CHANNEL OUT OF RANGE!");return KEYSIGHT_ERR;}
}

int KS33X00X_set_mod_parameters(const int serial_port, int source, double sample_rate, int number_of_samples, double t0, double t1, double t2, double order, char* waveform_name, double amplitude, char* error) {

    memset(error, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if (source == 1 || source == 2) {

        char error_msg[KEYSIGHT_MSG_SIZE];
        char msg[KEYSIGHT_MSG_SIZE];

        memset(msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));

        sprintf(msg, "SOURce%d:FUNCtion:ARBitrary:SRAT %0.6e\n", source, sample_rate);

        if (KS33X00X_write_msg_fast(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }

        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }

        memset(msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:DATA:VOLatile:CLEar\n", source);

        if (KS33X00X_write_msg_fast(serial_port, msg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }

        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }

        int length = 0;

        const int samples_per_second = (int)sample_rate;

        char arbFuncDataArray[(6 + 2) * number_of_samples];

        for (int s = 0; s < number_of_samples; ++s) {

            double t = (((double) s) / ((double)samples_per_second));

            double func_val = SF_Damped(t,t0, t1,t2,order);

            int adc_val = func_val > 0 ? (int) floor(func_val * 65535 / 2) : (int) ceil(func_val * 65535 / 2);

            char cval[KEYSIGHT_MSG_SIZE];
            memset(cval, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
            sprintf(cval, "%d", adc_val);

            if (length + strlen(cval) < sizeof(arbFuncDataArray)) {

                length += sprintf(arbFuncDataArray + length, "%s,", cval);
            }
        }

        size_t indexOfNullTerminator = strlen(arbFuncDataArray);

        arbFuncDataArray[indexOfNullTerminator - 1] = '\0'; // remove the trailing ','

        char lmsg[strlen(arbFuncDataArray)+KEYSIGHT_MSG_SIZE];
        memset(error_msg, '\0', sizeof(error_msg));
        memset(lmsg, '\0', (strlen(arbFuncDataArray)+KEYSIGHT_MSG_SIZE)*sizeof(char));
        sprintf(lmsg, "SOURce%d:DATA:ARBitrary:DAC %s, %s\n", source, waveform_name, arbFuncDataArray);

        if (KS33X00X_write_msg(serial_port, lmsg, error_msg)) {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_COM_ERR;
        }

        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }

        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(error_msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));

        sprintf(msg, "SOURce%d:FUNCtion ARB\n",source);
        if(KS33X00X_write_msg_fast(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}

        if (KS33X00X_get_error_msg(serial_port, error_msg)) {return KEYSIGHT_COM_ERR;}
        if (error_msg[1] != '0') {
            sprintf(error, "%s", error_msg);
            return KEYSIGHT_ERR;
        }

        memset(error_msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        sprintf(msg, "SOURce%d:FUNCtion:ARBitrary %s\n",source, waveform_name);
        if(KS33X00X_write_msg_fast(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}

        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}

        memset(error_msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
        memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));

        sprintf(msg, "SOURce%d:VOLTage %0.6E\n",source, amplitude);
        if(KS33X00X_write_msg_fast(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}

        if(KS33X00X_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
        if(error_msg[1] != '0'){sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}

        return KEYSIGHT_SUCCESS;
    } else {
        sprintf(error, "SET CHANNEL OUT OF RANGE!");
        return KEYSIGHT_ERR;
    }

}
