// todo: integrate into Keysight library
int KSE36XXA_set_voltage_range(const int serial_port, char* voltage_range)
{
    if (voltage_range != "HIGH" && voltage_range != "LOW")
    {
        printf("voltage range has to be eighter 'LOW' or 'HIGH' \n");
        return 1;
    }

    char scpi_msg[SCPI_MSG_SIZE];
    memset(&scpi_msg,'\0', sizeof(scpi_msg));
    sprintf(scpi_msg, "SOURce:VOLTage:RANGe %s\n", voltage_range);
    KSE36XXA_write_scpi(serial_port, scpi_msg);

    return 0;
}

// todo: integrate into Keysight library
int KSE36XXA_set_voltage_max(const int serial_port)
{
    char scpi_msg[SCPI_MSG_SIZE];
    memset(&scpi_msg,'\0', sizeof(scpi_msg));
    sprintf(scpi_msg, "SOURce:VOLTage:LEVel %s\n", "MAXimum");
    KSE36XXA_write_scpi(serial_port, scpi_msg);
    char* scpi_reply;
    do{
        KSE36XXA_write_scpi(serial_port, "*OPC?\n");
        scpi_reply = KSE36XXA_read_scpi(serial_port);
    }while(!(int)scpi_reply[0]);

    return 0;
}

// todo: integrate into Keysight library
int KSE36XXA_operation_complete(const int serial_port)
{
    char scpi_msg[] = {"*OPC?\n"};
    KSE36XXA_write_scpi(serial_port, scpi_msg);
    char* scpi_reply = KSE36XXA_read_scpi(serial_port);
    return (int)*scpi_reply;
}

