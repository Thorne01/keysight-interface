Headers, functions and example for running the DC power supply KS363XA.

The communication to the DC supplies are done via RS232. However, the Rx/Tx in this device is rewired around in order to work, see documentation. Therefore, the communication requires a timing delay in the read function which takes about <70 ms to complete. This needs to be considered when programming. This delay is performed on every function.

*OPC? Check is performed within the function after a command has been sent. This checks the device has read it, if 0 is returned then error is produced.

Note running the functions out of order given in the example could produce errors, this can be checks via error handling method. If error is returned then this will be the same as that given in the Keysight KS363XA documentation. 


In every function thats used it will always pass by reference error message which can be implimented however the user needs. All functions that require user input have the error check function built in, either custom error return or from the standard keysight documentation.

To produce the output for a given channel, the order in which the output_channel function is used is important. The commands for a given channel required has to follow the output_channel function for selected channel. For example: OUTPut 1; CURRent 5; OUTPut 2; CURRent 3. This will set current on channel 1 to 5A then set current on channel 2 to 3A. 

List of functions and purpose:

*****Communication functions*****

int KSE36XXA_write_msg(const int serial_port, char* msg, char* error_msg);
int KSE36XXA_read_msg(const int serial_port, char* reply, char* error_msg); //delay is written in here, settings to write is nested in a while loop.

int KSE36XXA_connect(const char* port_id, int data_bits, int parity, int parity_odd);
int KSE36XXA_disconnect(const int);

*****Check/initialising functions*****

int KSE36XXA_get_identify(const int serial_port, char* reply, char* error); //gets the device ID
int KSE36XXA_set_remote(const int serial_port, char* error); //sets the device to be controlled by PC
int KSE36XXA_set_local(const int serial_port, char* error); //sets the local control

int KSE36XXA_set_error_clear(const int serial_port, char* error); //if error occurs this will clear those messages
int KSE36XXA_get_error_msg(const int serial_port, char* reply); //gets the error message that is passed by reference to the char* error.
int KSE36XXA_set_clear_output_buffer(const int serial_port, char* error); //clears the buffer

int KSE36XXA_set_output_channel(const int serial_port, int output, char* error);//sets the channel for given commands that will follow this function

int KSE36XXA_get_output_channel(const int serial_port, int* channel, char* error); //getter for the channel currently selected

*****General functions*****

int KSE36XXA_set_reset(const int serial_port, char* error); //resets the device

int KSE36XXA_set_output(const int serial_port, int on_off, char* error); //turns the output on

int KSE36XXA_set_voltage(const int serial_port, double voltage, char* error); //set the voltage of the device
int KSE36XXA_set_voltage_step(const int serial_port, double voltage_step, char* error);//sets a given voltage step, this requires the command VOLT UP and VOLT DOWN to move between the given step... this still needs to be implimented.

int KSE36XXA_set_voltage_protection_state(const int serial_port, int state, char* error);//turns the voltage protection on
int KSE36XXA_set_voltage_protection_level(const int serial_port, double voltage_level, char* error);//sets the voltage protection level, in  voltages
int KSE36XXA_set_voltage_max(const int serial_port, char* error); //sets the max voltage for output (limit)
int KSE36XXA_set_voltage_range(const int serial_port, int voltage_range, char* error); //sets the range: {P8V |P20V}, low is 0 and 1 is high.

int KSE36XXA_get_voltage(const int serial_port, double* voltage, char* error);
int KSE36XXA_get_voltage_protection_state(const int serial_port, int* state, char* error);
int KSE36XXA_get_voltage_max(const int serial_port, double* voltage_max, char* error);
int KSE36XXA_get_voltage_range(const int serial_port, char* voltage_range, char* error);//getters for the settings above
int KSE36XXA_get_voltage_protection_trip(const int serial_port, int* state, char* error); //returns 1 if tripped

int KSE36XXA_set_OVP_clear(const int serial_port, char* error); //clears the over voltage protect warning, if over voltage is exceeded then the device stops operating, this function allows the device to operate again.
int KSE36XXA_set_OCP_clear(const int serial_port, char* error); //clears the ver current protection trip.

int KSE36XXA_set_current(const int serial_port, double current, char* error);//sets the current
int KSE36XXA_set_current_step(const int serial_port, double current_step, char* error);//sets the current step, this also requires more functions to use this... still to be programmed.

int KSE36XXA_set_current_protection_state(const int serial_port, int state, char* error);//set the protection state
int KSE36XXA_set_current_protection_level(const int serial_port, double current_level, char* error);//set the current to trip at
int KSE36XXA_set_current_max(const int serial_port, char* error);//set to the maximium current

int KSE36XXA_get_current(const int serial_port, double* current, char* error); //getter for the current
int KSE36XXA_get_current_max(const int serial_port, double* current_max, char* error); //gets the max set to
int KSE36XXA_get_current_protection_state(const int serial_port, int* state, char* error); //gets the state of the current protection
int KSE36XXA_get_current_protection_trip(const int serial_port, int* state, char* error);

int KSE36XXA_set_voltage_current(const int serial_port, double voltage, double current, char* error); //this will set the voltage and current at the same time.

int KSE36XXA_measure_voltage(const int serial_port, double* voltage, char* error);//the getters will send back the set voltage, this measures the actual voltage thats being passed by the device
int KSE36XXA_measure_current(const int serial_port, double* current, char* error);//same as measure_voltage but for the current

int KSE36XXA_debug_function(const int serial_port, char* reply, char* error);//function which a test command can be hardcoded in to it with error handling pre-written
