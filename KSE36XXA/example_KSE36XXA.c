#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "KSE36XXA.h"

void flush_stdin(void);
int enter_press();

int main(void)
{
	printf("\n +----------------------------------+");
	printf("\n |     KSE36XXA Serial Port Read    |");
	printf("\n +----------------------------------+\n\n");

	///////////Detector PS control code///////////

    int KS_detector_01 = KSE36XXA_connect("/dev/serial/by-id/usb-MOXA_Technologies_MOXA_UPort_1610-8-if00-port0", 7, 1, 0);

    int KS_detector_02 = KSE36XXA_connect("/dev/serial/by-id/usb-MOXA_Technologies_MOXA_UPort_1610-8-if00-port1", 7, 1, 0);

    ////////KS_detector_01 init////////

    char reply[KEYSIGHT_MSG_SIZE];
    char error[KEYSIGHT_MSG_SIZE];

    if (KSE36XXA_set_error_clear(KS_detector_01, error)) {
        printf("SET ERROR CLEAR: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if (KSE36XXA_set_clear_output_buffer(KS_detector_01, error)) {
        printf("SET CLEAR OUTPUT BUFFER: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if (KSE36XXA_set_remote(KS_detector_01, error)) {
        printf("SET REMOTE: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if (KSE36XXA_set_reset(KS_detector_01, error)) {
        printf("SET RESET: %s\n", error);
        return KEYSIGHT_ERR;
    }

    sleep(1);

    if(KSE36XXA_set_voltage_protection_level(KS_detector_01, 6, error))
    {
        printf("SET VOLTAGE PROTECTION LEVEL: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_voltage_protection_state(KS_detector_01, 1, error))
    {
        printf("SET VOLTAGE PROTECTION STATE: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_current_protection_level(KS_detector_01, 2.6, error))
    {
        printf("SET VOLTAGE PROTECTION LEVEL: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_current_protection_state(KS_detector_01, 1, error))
    {
        printf("SET VOLTAGE PROTECTION STATE: %s", error);
        return KEYSIGHT_ERR;
    }

    if (KSE36XXA_set_voltage_range(KS_detector_01, 1, error)) {
        printf("SET VOLTAGE RANGE: %s\n", error);
        KSE36XXA_get_error_msg(KS_detector_01, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_voltage(KS_detector_01, 5, error))
    {
        printf("SET VOLTAGE: %s\n", error);
        return KEYSIGHT_ERR;
    }

    int voltage_state;
    if(KSE36XXA_get_voltage_protection_trip(KS_detector_01, &voltage_state, error))
    {
        printf("GET VOLTAGE PROTECTION TRIP: %s\n", error);
        return KEYSIGHT_ERR;
    }
    if(voltage_state){printf("KS tripping on init!\n"); return -1;}

    int current_state;
    if(KSE36XXA_get_current_protection_trip(KS_detector_01, &current_state, error))
    {
        printf("GET CURRENT PROTECTION TRIP: %s\n", error);
        return KEYSIGHT_ERR;
    }
    if(current_state){printf("KS tripping on init!\n"); return -1;}

    ////////KS_detector_02 init////////

    if (KSE36XXA_set_error_clear(KS_detector_02, error)) {
        printf("SET ERROR CLEAR: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if (KSE36XXA_set_clear_output_buffer(KS_detector_02, error)) {
        printf("SET CLEAR OUTPUT BUFFER: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if (KSE36XXA_set_remote(KS_detector_02, error)) {
        printf("SET REMOTE: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if (KSE36XXA_set_reset(KS_detector_02, error)) {
        printf("SET RESET: %s\n", error);
        return KEYSIGHT_ERR;
    }

    sleep(1);

    if(KSE36XXA_set_voltage_protection_level(KS_detector_02, 6, error))
    {
        printf("SET VOLTAGE PROTECTION LEVEL: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_voltage_protection_state(KS_detector_02, 1, error))
    {
        printf("SET VOLTAGE PROTECTION STATE: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_current_protection_level(KS_detector_02, 0.4, error))
    {
        printf("SET VOLTAGE PROTECTION LEVEL: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_current_protection_state(KS_detector_02, 1, error))
    {
        printf("SET VOLTAGE PROTECTION STATE: %s", error);
        return KEYSIGHT_ERR;
    }

    if (KSE36XXA_set_voltage_range(KS_detector_02, 1, error)) {
        printf("SET VOLTAGE RANGE: %s\n", error);
        KSE36XXA_get_error_msg(KS_detector_01, error);
        printf("ERROR DEVICE CHECK: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_voltage(KS_detector_02, 5, error))
    {
        printf("SET VOLTAGE: %s\n", error);
        return KEYSIGHT_ERR;
    }

    int voltage_state_2;
    if(KSE36XXA_get_voltage_protection_trip(KS_detector_02, &voltage_state_2, error))
    {
        printf("GET VOLTAGE PROTECTION TRIP: %s\n", error);
        return KEYSIGHT_ERR;
    }
    if(voltage_state){printf("KS tripping on init!\n"); return -1;}

    int current_state_2;
    if(KSE36XXA_get_current_protection_trip(KS_detector_02, &current_state_2, error))
    {
        printf("GET CURRENT PROTECTION TRIP: %s\n", error);
        return KEYSIGHT_ERR;
    }
    if(current_state){printf("KS tripping on init!\n"); return -1;}

    ////////event readout////////

    while(!enter_press())
    {
        if(KSE36XXA_get_current_protection_trip(KS_detector_01, &current_state, error))
        {
            printf("GET CURRENT PROTECTION TRIP: %s\n", error);
            return KEYSIGHT_ERR;
        }
        if(current_state)
        {
            //for midas add alarm. Not clear if need to add FE failure or then update function to restart the PS?
            return -1;
        }

        if(KSE36XXA_get_voltage_protection_trip(KS_detector_01, &voltage_state, error))
        {
            printf("GET VOLTAGE PROTECTION TRIP: %s\n", error);
            return KEYSIGHT_ERR;
        }
        if(voltage_state)
        {
            //for midas add alarm. Not clear if need to add FE failure or then update function to restart the PS?
            return -1;
        }

        if(KSE36XXA_get_current_protection_trip(KS_detector_02, &current_state_2, error))
        {
            printf("GET CURRENT PROTECTION TRIP: %s\n", error);
            return KEYSIGHT_ERR;
        }
        if(current_state_2)
        {
            //for midas add alarm. Not clear if need to add FE failure or then update function to restart the PS?
            return -1;
        }

        if(KSE36XXA_get_voltage_protection_trip(KS_detector_02, &voltage_state_2, error))
        {
            printf("GET VOLTAGE PROTECTION TRIP: %s\n", error);
            return KEYSIGHT_ERR;
        }
        if(voltage_state_2)
        {
            //for midas add alarm. Not clear if need to add FE failure or then update function to restart the PS?
            return -1;
        }
    }

    ////////closing FE////////

    if(KSE36XXA_set_clear_output_buffer(KS_detector_01, error))
    {
        printf("SET CLEAR OUTPUT BUFFER: %s\n", error);
        KSE36XXA_get_error_msg(KS_detector_01, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_clear_output_buffer(KS_detector_02, error))
    {
        printf("SET CLEAR OUTPUT BUFFER: %s\n", error);
        KSE36XXA_get_error_msg(KS_detector_02, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_local(KS_detector_01, error))
    {
        printf("SET LOCAL: %s\n", error);
        KSE36XXA_get_error_msg(KS_detector_01, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_local(KS_detector_02, error))
    {
        printf("SET LOCAL: %s\n", error);
        KSE36XXA_get_error_msg(KS_detector_02, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }

    KSE36XXA_disconnect(KS_detector_01);
    KSE36XXA_disconnect(KS_detector_02);
}

    //01 = 5 V, 25 V range OVP = ?, OCP 2.6 A
    //02 = 5 V, 8 V range, OVP = ?, OCP = ?

	/*------ Opening the Serial Port -----*/
	/*int fd = KSE36XXA_connect("/dev/ttyUSB0", 7, 1, 0);
	if(fd < 1)

	{
		printf("BAD FILE DESCRIPTOR\n");
		return -1;		
	}

	printf("connect worked\n");

    char reply[KEYSIGHT_MSG_SIZE];
	char error[KEYSIGHT_MSG_SIZE];

    if(KSE36XXA_set_error_clear(fd, error))
    {
        printf("SET ERROR CLEAR: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_clear_output_buffer(fd, error))
    {
        printf("SET CLEAR OUTPUT BUFFER: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_remote(fd, error))
    {
        printf("SET REMOTE: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_reset(fd, error))
    {
        printf("SET RESET: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_get_identify(fd, reply, error))
    {
        printf("GET IDENTIFY: %s\n", error);
        KSE36XXA_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }
    else {printf("ID: %s",reply);}

    if(KSE36XXA_set_output_channel(fd, 2, error))
    {
        printf("SET OUTPUT CHANNEL: %s\n", error);
        return KEYSIGHT_ERR;
    }

    int output_channel = 0;
    if(KSE36XXA_get_output_channel(fd, &output_channel, error))
    {
        printf("GET OUTPUT CHANNEL: %s\n", error);
        return KEYSIGHT_ERR;
    }
    printf("Output channel: %c\n",output_channel);

    if(KSE36XXA_set_voltage_range(fd, 0, error))
    {
        printf("SET VOLTAGE RANGE: %s\n", error);
        KSE36XXA_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_voltage_max(fd, error))
    {
        printf("SET VOLTAGE MAX: %s\n", error);
        KSE36XXA_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }

    double voltage_max;
    if(KSE36XXA_get_voltage_max(fd, &voltage_max, error))
    {
        printf("GET VOLTAGE MAX: %s\n", error);
        KSE36XXA_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }
    printf("%f\n", voltage_max);

    char voltage_range[KEYSIGHT_MSG_SIZE];
    if(KSE36XXA_get_voltage_range(fd, voltage_range, error))
    {
        printf("GET VOLTAGE RANGE: %s\n", error);
        KSE36XXA_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }
    printf("%s", voltage_range);

    double voltage = 8;
    double current = 2.5;
    if(KSE36XXA_set_voltage(fd, voltage, error))
    {
        printf("SET VOLTAGE: %s\n", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_current(fd, current, error))
    {
        printf("SET CURRENT: %s\n", error);
        return KEYSIGHT_ERR;
    }
    printf("Set voltage: %f V\tSet current: %f A\n", voltage, current);

    printf("Voltage: %f V, Current: %f A\n", voltage, current);
    if(KSE36XXA_set_voltage_current(fd, voltage, current, error)) {
        printf("SET VOLTAGE/CURRENT: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_output(fd, 1, error))
    {
        printf("SET OUTPUT: %s", error);
        return KEYSIGHT_ERR;
    }

    double get_voltage;
    double get_current;
    if(KSE36XXA_get_voltage(fd, &get_voltage, error))
    {
        printf("GET VOLTAGE: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_get_current(fd, &get_current, error))
    {
        printf("GET CURRENT: %s", error);
        return KEYSIGHT_ERR;
    }
    printf("Get voltage: %f V\tGet current: %f A\n", get_voltage, get_current);

    if(KSE36XXA_set_voltage_protection_state(fd, 1, error))
    {
        printf("SET VOLTAGE PROTECTION STATE: %s", error);
        return KEYSIGHT_ERR;
    }
    if(KSE36XXA_set_voltage_protection_level(fd, 20, error))
    {
        printf("SET VOLTAGE PROTECTION LEVEL: %s", error);
        return KEYSIGHT_ERR;
    }

    int over_volt_state = 0;

    if(KSE36XXA_get_voltage_protection_state(fd, &over_volt_state, error))
    {
        printf("GET VOLTAGE PROTECTION STATE: %s", error);
        return KEYSIGHT_ERR;
    }
    printf("Voltage protection state: %d\n", over_volt_state);

    if(KSE36XXA_set_OVP_clear(fd, error))
    {
        printf("SET OVP CLEAR: %s", error);
        return KEYSIGHT_ERR;
    }

    double measure_voltage =0;
    double measure_current =0;

    while(!enter_press())
    {
        if (KSE36XXA_measure_voltage(fd, &measure_voltage, error)) {
            printf("MEASURE VOLTAGE: %s",
                   error);
            return KEYSIGHT_ERR;
        }

        if (KSE36XXA_measure_current(fd, &measure_current, error)) {
            printf("MEASURE CURRENT: %s",
                   error);
            return KEYSIGHT_ERR;
        }

        printf("Measure voltage: %f V\tMeasure current: %f A\r",measure_voltage, measure_current);
        fflush(stdout);
    }
    if(KSE36XXA_set_output(fd, 0, error))
    {
        printf("SET OUTPUT: %s", error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_clear_output_buffer(fd, error))
    {
        printf("SET CLEAR OUTPUT BUFFER: %s\n", error);
        KSE36XXA_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }

    if(KSE36XXA_set_local(fd, error))
    {
        printf("SET LOCAL: %s\n", error);
        KSE36XXA_get_error_msg(fd, error);
        printf("ERROR DEVICE CHECK: %s",error);
        return KEYSIGHT_ERR;
    }
	KSE36XXA_disconnect(fd);
}*/

void flush_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int enter_press()
{
    int stdin_value = 0;
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    stdin_value = FD_ISSET(STDIN_FILENO, &fds);
    if (stdin_value != 0)
        flush_stdin();
    return stdin_value;
}