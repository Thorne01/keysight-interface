/******************************************************************************
** Written by: Z. Hodge
** Date: 11 July 2019
**
**
** NOTE: The KSE36XXA REQUIRES the use of DSRDTR hardware flow control.
** Linux does not support DSRDTR flow control, but it can be imitated
** using RTSCTS and a special connector.
** This library assumes the use of RTSCTS over DSRDTR lines using
** a special RS-232 crossover connector.
******************************************************************************/

#ifndef __KSE36XXAH__
#define __KSE36XXAH__

#define KEYSIGHT_MSG_SIZE 512
#define KEYSIGHT_COM_ERR 1
#define KEYSIGHT_SUCCESS 0
#define KEYSIGHT_ERR 1

int KSE36XXA_write_msg(const int serial_port, char* msg, char* error_msg);
int KSE36XXA_read_msg(const int serial_port, char* reply, char* error_msg);

int KSE36XXA_connect(const char* port_id, int data_bits, int parity, int parity_odd);
int KSE36XXA_disconnect(const int);

int KSE36XXA_get_identify(const int serial_port, char* reply, char* error);
int KSE36XXA_set_remote(const int serial_port, char* error);
int KSE36XXA_set_local(const int serial_port, char* error);
int KSE36XXA_set_error_clear(const int serial_port, char* error);

int KSE36XXA_set_reset(const int serial_port, char* error);
int KSE36XXA_get_error_msg(const int serial_port, char* reply);
int KSE36XXA_set_clear_output_buffer(const int serial_port, char* error);

int KSE36XXA_set_voltage_current(const int serial_port, double voltage, double current, char* error);

int KSE36XXA_set_output(const int serial_port, int on_off, char* error);

int KSE36XXA_set_voltage(const int serial_port, double voltage, char* error);
int KSE36XXA_set_voltage_step(const int serial_port, double voltage_step, char* error);
int KSE36XXA_set_voltage_protection_state(const int serial_port, int state, char* error);
int KSE36XXA_set_voltage_protection_level(const int serial_port, double voltage_level, char* error);
int KSE36XXA_set_voltage_max(const int serial_port, char* error);
int KSE36XXA_set_voltage_range(const int serial_port, int voltage_range, char* error);

int KSE36XXA_set_current(const int serial_port, double current, char* error);
int KSE36XXA_set_current_step(const int serial_port, double current_step, char* error);

int KSE36XXA_set_current_protection_state(const int serial_port, int state, char* error);
int KSE36XXA_set_current_protection_level(const int serial_port, double current_level, char* error);
int KSE36XXA_set_current_max(const int serial_port, char* error);

int KSE36XXA_get_voltage(const int serial_port, double* voltage, char* error);
int KSE36XXA_get_current(const int serial_port, double* current, char* error);
int KSE36XXA_get_voltage_protection_state(const int serial_port, int* state, char* error);
int KSE36XXA_get_current_protection_state(const int serial_port, int* state, char* error);

int KSE36XXA_get_voltage_max(const int serial_port, double* voltage_max, char* error);
int KSE36XXA_get_current_max(const int serial_port, double* current_max, char* error);
int KSE36XXA_get_voltage_range(const int serial_port, char* voltage_range, char* error);
int KSE36XXA_get_voltage_protection_trip(const int serial_port, int* state, char* error);
int KSE36XXA_get_current_protection_trip(const int serial_port, int* state, char* error);

int KSE36XXA_measure_voltage(const int serial_port, double* voltage, char* error);
int KSE36XXA_measure_current(const int serial_port, double* current, char* error);

int KSE36XXA_set_output_channel(const int serial_port, int output, char* error);
int KSE36XXA_get_output_channel(const int serial_port, int* channel, char* error);

int KSE36XXA_set_OVP_clear(const int serial_port, char* error);
int KSE36XXA_set_OCP_clear(const int serial_port, char* error);

int KSE36XXA_debug_function(const int serial_port, char* reply, char* error);
#endif
