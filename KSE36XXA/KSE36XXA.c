#include "KSE36XXA.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

int KSE36XXA_connect(const char* port_id, int data_bits, int parity, int parity_odd)
{
    int serial_port = open(port_id, (O_RDWR | O_NOCTTY  | O_SYNC) );
	if(serial_port == -1)                        // Error Checking 
	{
		//printf("\n  Error! in Opening %s \n",port_id);
		//printf("__LINE__ = %d, error %s\n", __LINE__, strerror(errno));
	}

	   //printf("\n  %s Opened Successfully \n",port_id);

	// configure port

	struct termios options;
	memset(&options, 0, sizeof(options));  /* clear the new struct */

	options.c_cflag = 0x00;
	options.c_oflag = 0x00;
	options.c_lflag = 0x00;
	options.c_iflag = 0x00;

	options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
    options.c_oflag = 0x00;
	options.c_lflag &= ~(ECHO | ECHONL | IEXTEN );
	options.c_lflag |= ICANON | ISIG;

	// options.c_lflag &= ~ICANON;
	options.c_cflag |= CSTOPB | CLOCAL | CREAD | CRTSCTS;
	switch(data_bits)
	{
		case 7:
			options.c_cflag |= CS7;
			break;
		case 8:
		default:
			options.c_cflag |= CS8;
			break;
	}

	switch(parity)
	{
		case 1:
			options.c_cflag |= PARENB;
			break;
		case 0:
		default:
			options.c_cflag &= ~PARENB;
			break;
	}

	switch(parity_odd)
	{
		case 1:
			options.c_cflag |= PARODD;
			break;
		case 0:
		default:
			options.c_cflag &= ~PARODD;
			break;
	}
    options.c_cc[VMIN]  = 0;
    options.c_cc[VTIME] = 0;
    options.c_cc[VEOL] = 0;
    options.c_cc[VEOL2] = 0;
    options.c_cc[VEOF] = 0x04;

	cfsetispeed(&options, B9600);
	cfsetospeed(&options, B9600);
	tcflush(serial_port, TCIFLUSH);
	tcflush(serial_port, TCOFLUSH);

	if(tcsetattr(serial_port, TCSANOW, &options) != 0)
  	{
		//printf("__LINE__ = %d, error %s\n", __LINE__, strerror(errno));
		return -1;
    }
	return serial_port;
}

int KSE36XXA_write_msg(const int serial_port, char* msg, char* error_msg)
{
	unsigned int status = 0x00;
    ioctl(serial_port, TIOCMGET, &status);
    //printf("SENDING: %s\n",scpi_msg);
    int nbytes_written = write(serial_port, msg, strlen(msg));
    if(nbytes_written != strlen(msg))
    {sprintf(error_msg,"WRITE ERROR!\n");return KEYSIGHT_COM_ERR;} //error check
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_read_msg(const int serial_port, char* reply, char* error_msg)
{
    memset(reply, '\0', sizeof(char)*KEYSIGHT_MSG_SIZE);
	int bytes_available = 0;
    int msec = 0, trigger = 5000; /* 5s to wait before timeout */
    clock_t before = clock();
	while(bytes_available<1)
	{
        clock_t difference = clock() - before;
        msec = difference * 1000 / CLOCKS_PER_SEC; //50-60 msec delay for communication
	    ioctl(serial_port, FIONREAD, &bytes_available);
	    if(msec > trigger)
	    {
            sprintf(error_msg, "ERROR! READ TIMEOUT\n");
            return KEYSIGHT_COM_ERR;
	    }
	}
    read(serial_port, reply, bytes_available);
	return KEYSIGHT_SUCCESS;
}

int KSE36XXA_set_error_clear(const int serial_port, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "*CLS\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "ERROR CLEAR COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_get_error_msg(const int serial_port, char* reply)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "SYSTem:ERRor?\n", error_msg)){sprintf(reply,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(reply,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*CLS\n", error_msg)){sprintf(reply,"ERROR MESSAGE FLUSH: %s\n",error_msg);return KEYSIGHT_COM_ERR;}
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_set_clear_output_buffer(const int serial_port, char* error)
{
	char error_msg[KEYSIGHT_MSG_SIZE];
	char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "\x03\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "CLEAR BUFFER COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_disconnect(const int serial_port)
{
	return close(serial_port);
}

int KSE36XXA_set_reset(const int serial_port, char* error)
{
	char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "*RST\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "RESET COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_get_identify(const int serial_port, char* reply, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "*IDN?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    return KEYSIGHT_SUCCESS;
}
int KSE36XXA_set_remote(const int serial_port, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "SYSTem:REMote\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET REMOTE COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_set_local(const int serial_port, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "SYSTem:LOCal\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET LOCAL COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_set_voltage_current(const int serial_port, double voltage, double current, char* error)
{
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	sprintf(msg, "APPLy %0.4E, %0.4E\n",voltage, current);
    if(KSE36XXA_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET VOLTAGE/CURRENT COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    if(KSE36XXA_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
    if(error_msg[1] == '0'){return KEYSIGHT_SUCCESS;}
    else{sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
}

int KSE36XXA_set_output(const int serial_port, int on_off, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
	char msg[KEYSIGHT_MSG_SIZE];
	char state[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
	if(on_off > 1 || on_off < 0){sprintf(error, "ON/OFF COMMAND OUT OF RANGE!\n");return KEYSIGHT_ERR;}
	if(on_off == 0){sprintf(state, "OFF");}
    else{sprintf(state, "ON");}
	memset(msg,'\0', sizeof(msg));
	sprintf(msg, "OUTPut:STATe %s\n",state);
    if(KSE36XXA_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET OUTPUT COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    return KEYSIGHT_SUCCESS; //could add error handle functioning here as the check of error handling in example will not work if something goes wrong. If error handling is added here then the same can be done for every other function.
}

int KSE36XXA_set_voltage(const int serial_port, double voltage, char* error)
{
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(msg,'\0', sizeof(msg));
    sprintf(msg, "SOURce:VOLTage:LEVel %0.4E\n",voltage);
    if(KSE36XXA_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET VOLTAGE COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    if(KSE36XXA_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
    if(error_msg[1] == '0'){return KEYSIGHT_SUCCESS;}
    else{sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
}

int KSE36XXA_set_voltage_max(const int serial_port, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "SOURce:VOLTage:LEVel MAXimum\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET VOLTAGE MAX COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_get_voltage_max(const int serial_port, double* voltage_max, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "VOLT? MAX\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    *voltage_max = strtod(reply, NULL);
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_set_voltage_step(const int serial_port, double voltage_step, char* error)
{
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(msg,'\0', sizeof(msg));
    sprintf(msg, "SOURce:VOLTage:LEVel %0.4E\n",voltage_step);
    if(KSE36XXA_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET VOLTAGE STEP COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    if(KSE36XXA_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
    if(error_msg[1] == '0'){return KEYSIGHT_SUCCESS;}
    else{sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
}

int KSE36XXA_set_voltage_protection_state(const int serial_port, int state, char* error) {
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "SOURce:VOLTage:PROTection:STATe %d\n", state);
    if (KSE36XXA_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_read_msg(serial_port, reply, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (reply[0] != '1') {
        sprintf(error, "SET VOLTAGE PROTECTION STATE COMMAND NOT EXECUTED!\n");
        return KEYSIGHT_ERR;
    }
    if (KSE36XXA_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] == '0') { return KEYSIGHT_SUCCESS; }
    else {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
}

int KSE36XXA_get_voltage_protection_state(const int serial_port, int* state, char* error) {
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "SOURce:VOLTage:PROTection:STATe?\n");
    if (KSE36XXA_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_read_msg(serial_port, reply, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    sscanf(reply,"%d",state);
    if (KSE36XXA_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] == '0') { return KEYSIGHT_SUCCESS; }
    else {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
}
int KSE36XXA_set_voltage_protection_level(const int serial_port, double voltage_level, char* error) {
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "SOURce:VOLTage:PROTection:LEVel %0.4E\n", voltage_level);
    if (KSE36XXA_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_read_msg(serial_port, reply, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (reply[0] != '1') {
        sprintf(error, "SET VOLTAGE PROTECTION LEVEL COMMAND NOT EXECUTED!\n");
        return KEYSIGHT_ERR;
    }
    if (KSE36XXA_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] == '0') { return KEYSIGHT_SUCCESS; }
    else {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
}

int KSE36XXA_set_voltage_range(const int serial_port, int voltage_range, char* error)
{
    char range_msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(range_msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(voltage_range != 1 && voltage_range !=0)
    {
        sprintf(error, "SET VOLTAGE RANGE: IS 0 = LOW, 1 = HIGH\n");
        return KEYSIGHT_ERR;
    }

    if (voltage_range != 0)
    {
        sprintf(range_msg, "HIGH");
    }
    else{sprintf(range_msg, "LOW");}

    memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    sprintf(msg, "SOURce:VOLTage:RANGe %s\n", range_msg);
    if(KSE36XXA_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}

    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET VOLTAGE RANGE COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_get_voltage_range(const int serial_port, char* voltage_range, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(voltage_range, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "VOLTage:RANGe?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,voltage_range,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_set_current(const int serial_port, double current, char* error)

{
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    sprintf(msg, "SOURce:CURRent:LEVel %0.4E\n",current);
    if(KSE36XXA_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET CURRENT COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    if(KSE36XXA_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
    if(error_msg[1] == '0'){return KEYSIGHT_SUCCESS;}
    else{sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
}

int KSE36XXA_set_current_step(const int serial_port, double current_step, char* error)
{
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    sprintf(msg, "SOURce:CURRent:LEVel %0.4E\n",current_step);
    if(KSE36XXA_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET CURRENT STEP COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    if(KSE36XXA_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
    if(error_msg[1] == '0'){return KEYSIGHT_SUCCESS;}
    else{sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
}

int KSE36XXA_set_current_max(const int serial_port, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "SOURce:CURRent:LEVel MAXimum\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET CURRENT RANGE COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_get_current_max(const int serial_port, double* current_max, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "CURR? MAX\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    *current_max = strtod(reply, NULL);
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_set_current_protection_state(const int serial_port, int state, char* error) {
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "SOURce:CURRent:PROTection:STATe %d\n", state);
    if (KSE36XXA_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_read_msg(serial_port, reply, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (reply[0] != '1') {
        sprintf(error, "SET CURRENT PROTECTION STATE COMMAND NOT EXECUTED!\n");
        return KEYSIGHT_ERR;
    }
    if (KSE36XXA_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] == '0') { return KEYSIGHT_SUCCESS; }
    else {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
}

int KSE36XXA_get_current_protection_state(const int serial_port, int* state, char* error) {
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "SOURce:CURRent:PROTection:STATe?\n");
    if (KSE36XXA_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_read_msg(serial_port, reply, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    sscanf(reply,"%d",state);
    if (KSE36XXA_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] == '0') { return KEYSIGHT_SUCCESS; }
    else {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
}

int KSE36XXA_set_current_protection_level(const int serial_port, double current_level, char* error) {
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "SOURce:CURRent:PROTection:LEVel %0.4E\n", current_level);
    if (KSE36XXA_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_read_msg(serial_port, reply, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (reply[0] != '1') {
        sprintf(error, "SET CURRENT PROTECTION LEVEL COMMAND NOT EXECUTED!\n");
        return KEYSIGHT_ERR;
    }
    if (KSE36XXA_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] == '0') { return KEYSIGHT_SUCCESS; }
    else {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
}

int KSE36XXA_set_OCP_clear(const int serial_port, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "CURR:PROT:CLE\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET OCP CLEAR COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_set_OVP_clear(const int serial_port, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "VOLT:PROT:CLE\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET OVP CLEAR COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    else{return KEYSIGHT_SUCCESS;}
}

int KSE36XXA_get_voltage(const int serial_port, double* voltage, char* error)
{
	char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "SOURce:VOLTage?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    *voltage = strtod(reply, NULL);
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_get_current(const int serial_port, double* current, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "SOURce:CURRent?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    *current = strtod(reply, NULL);
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_get_voltage_protection(const int serial_port, int* on_off, double* voltage, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "SOURce:VOLTage:PROTection:STATe?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    sscanf(reply,"%d",on_off);
    if(reply[0] == '1')
    {
        if(KSE36XXA_write_msg(serial_port, "SOURce:VOLTage:PROTection:LEVel?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
        *voltage = strtod(reply, NULL);
    }
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_measure_current(const int serial_port, double* current, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "MEASure:CURRent:DC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    *current = strtod(reply, NULL);
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_measure_voltage(const int serial_port, double* voltage, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "MEASure:VOLTage?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    *voltage = strtod(reply, NULL);
    return KEYSIGHT_SUCCESS;
}

int KSE36XXA_set_output_channel(const int serial_port, int output, char* error)
{
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(msg,'\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    sprintf(msg, "INSTrument OUTPut%d\n",output);
    printf("%s",msg);
    if(KSE36XXA_write_msg(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_write_msg(serial_port, "*OPC?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(reply[0] != '1'){sprintf(error, "SET OUTPUT CHANNEL COMMAND NOT EXECUTED!\n"); return KEYSIGHT_ERR;}
    if(KSE36XXA_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
    if(error_msg[1] == '0'){return KEYSIGHT_SUCCESS;}
    else{sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
}

int KSE36XXA_get_output_channel(const int serial_port, int* channel, char* error)
{
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE *sizeof(char));
    if(KSE36XXA_write_msg(serial_port, "INSTrument?\n", error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    if(KSE36XXA_read_msg(serial_port,reply,error_msg)){sprintf(error,"%s",error_msg);return KEYSIGHT_COM_ERR;}
    *channel = reply[4];
    if(KSE36XXA_get_error_msg(serial_port, error_msg)){return KEYSIGHT_COM_ERR;}
    if(error_msg[1] == '0'){return KEYSIGHT_SUCCESS;}
    else{sprintf(error,"%s",error_msg);return KEYSIGHT_ERR;}
}

int KSE36XXA_get_voltage_protection_trip(const int serial_port, int* state, char* error)
{
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "VOLTage:PROTection:TRIPped?\n");
    if (KSE36XXA_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_read_msg(serial_port, reply, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    sscanf(reply,"%d",state);
    if (KSE36XXA_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] == '0') { return KEYSIGHT_SUCCESS; }
    else {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
}

int KSE36XXA_get_current_protection_trip(const int serial_port, int* state, char* error)
{
    char msg[KEYSIGHT_MSG_SIZE];
    char error_msg[KEYSIGHT_MSG_SIZE];
    char reply[KEYSIGHT_MSG_SIZE];
    memset(reply, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(error_msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    memset(msg, '\0', KEYSIGHT_MSG_SIZE * sizeof(char));
    sprintf(msg, "CURRent:PROTection:TRIPped?\n");
    if (KSE36XXA_write_msg(serial_port, msg, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    if (KSE36XXA_read_msg(serial_port, reply, error_msg)) {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_COM_ERR;
    }
    sscanf(reply,"%d",state);
    if (KSE36XXA_get_error_msg(serial_port, error_msg)) { return KEYSIGHT_COM_ERR; }
    if (error_msg[1] == '0') { return KEYSIGHT_SUCCESS; }
    else {
        sprintf(error, "%s", error_msg);
        return KEYSIGHT_ERR;
    }
}
